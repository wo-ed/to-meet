package st.alker.tomeet.alarm;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import st.alker.tomeet.geofence.GeoService;

/**
 * Starts the alarm after a device reboot
 * also starts geofences after reboot
 */
public class BootBroadcastReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Intent alarmIntent = new Intent(context, AlarmService.class);
        alarmIntent.putExtra("type", "bootAlarm");
        context.startService(alarmIntent);

        Intent geoIntent = new Intent(context, GeoService.class);
        geoIntent.putExtra("type", "geoBoot");
        context.startService(geoIntent);
    }
}
