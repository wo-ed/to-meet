package st.alker.tomeet.rest.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

// If needed create separate classes for get/post/put/delete (different parameters)
public class Device implements Serializable {
    @SerializedName("Id")
    private int id;
    @SerializedName("UserName")
    private String userName;
    @SerializedName("MACBluetooth")
    private String macBluetooth;
    @SerializedName("LastLatitude")
    private String lastLatitude;
    @SerializedName("LastLongitude")
    private String lastLongitude;
    @SerializedName("LastDate")
    private Date lastDate;
    @SerializedName("FcmToken")
    private String fcmToken;

    public Device() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMacBluetooth() {
        return macBluetooth;
    }

    public void setMacBluetooth(String macBluetooth) {
        this.macBluetooth = macBluetooth;
    }

    public String getLastLatitude() {
        return lastLatitude;
    }

    public void setLastLatitude(String lastLatitude) {
        this.lastLatitude = lastLatitude;
    }

    public String getLastLongitude() {
        return lastLongitude;
    }

    public void setLastLongitude(String lastLongitude) {
        this.lastLongitude = lastLongitude;
    }

    public Date getLastDate() {
        return lastDate;
    }

    public void setLastDate(Date lastDate) {
        this.lastDate = lastDate;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
