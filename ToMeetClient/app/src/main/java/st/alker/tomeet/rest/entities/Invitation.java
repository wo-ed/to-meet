package st.alker.tomeet.rest.entities;

import com.google.gson.annotations.SerializedName;

// If needed create separate classes for get/post/put/delete (different parameters)
public class Invitation {
    @SerializedName("Id")
    private int id;
    @SerializedName("EventId")
    private int eventId;
    @SerializedName("Event")
    private Event event;
    @SerializedName("GuestUserName")
    private String guestUserName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEventId() {
        return eventId;
    }

    public Event getEvent() {
        return event;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public String getGuestUserName() {
        return guestUserName;
    }

    public void setGuestUserName(String guestUserName) {
        this.guestUserName = guestUserName;
    }
}
