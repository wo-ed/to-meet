package st.alker.tomeet.rest;

import android.content.Context;
import android.util.Log;
import com.google.gson.Gson;
import com.loopj.android.http.*;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import static st.alker.tomeet.rest.Constants.MimeTypes.JSON;
import java.nio.charset.UnsupportedCharsetException;

import static cz.msebera.android.httpclient.entity.ContentType.APPLICATION_JSON;

public class GsonHttpClient extends AsyncHttpClient {
    private static final String TAG = "GsonHttpClient";

    private Gson gson;

    public GsonHttpClient() {
        this(new Gson());
    }

    public GsonHttpClient(Gson gson) {
        setGson(gson);
    }

    public <T> RequestHandle getObjects(Context context, String url, Header[] headers, RequestParams params, GsonHttpResponseHandler<T[]> responseHandler) {
        return super.get(context, url, headers, params, responseHandler);
    }

    public <T> RequestHandle getObject(Context context, String url, long id, Header[] headers, RequestParams params, GsonHttpResponseHandler<T> responseHandler) {
        url = getResourceUrl(url, id);
        return super.get(context, url, headers, params, responseHandler);
    }

    public RequestHandle postObject(Context context, String url, Header[] headers, Object inputParameterObject, TextHttpResponseHandler responseHandler) {
        StringEntity entity = getStringEntity(inputParameterObject);
        return super.post(context, url, headers, entity, JSON, responseHandler);
    }

    public RequestHandle putObject(Context context, String url, long id, Header[] headers, Object inputParameterObject, TextHttpResponseHandler responseHandler) {
        url = getResourceUrl(url, id);
        StringEntity entity = getStringEntity(inputParameterObject);
        return super.put(context, url, headers, entity, JSON, responseHandler);
    }

    public RequestHandle deleteObject(Context context, String url, long id, Header[] headers, RequestParams params, TextHttpResponseHandler responseHandler) {
        url = getResourceUrl(url, id);
        return super.delete(context, url, headers, params, responseHandler);
    }

    private String getResourceUrl(String url, long id) {
        String suffix = "/" + id;
        if (url.endsWith(suffix)) {
            return url;
        }
        return url + suffix;
    }

    private StringEntity getStringEntity(Object inputParameterObj) {
        try {
            String json = gson.toJson(inputParameterObj);
            return new StringEntity(json, APPLICATION_JSON);
        } catch (UnsupportedCharsetException e) {
            Log.e(TAG, "String entity could not be created", e);
            return null;
        }
    }


    public Gson getGson() {
        return gson;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }
}
