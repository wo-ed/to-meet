package st.alker.tomeet.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import st.alker.tomeet.Globals;
import st.alker.tomeet.R;
import st.alker.tomeet.adapters.GuestsAdapter;
import st.alker.tomeet.rest.entities.LoginResponse;

import java.util.ArrayList;
import java.util.Collection;

public class GuestsFragment extends Fragment {
    private static final String BUNDLE_KEY_EDITABLE = "EDITABLE";

    private GuestsAdapter adapter;
    private RecyclerView recyclerView;
    private ImageButton addButton;
    private EditText textNewGuest;
    private LoginResponse login;
    private boolean editable = false;

    private ArrayList<String> guests = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null) {
            editable = savedInstanceState.getBoolean(BUNDLE_KEY_EDITABLE);
        }

        adapter = new GuestsAdapter(getContext(), guests, editable);
        login = Globals.ENCRYPTED_STORAGE.get(getString(R.string.storage_key_login_data));
    }

    public GuestsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_guests, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.list_guests);
        addButton = (ImageButton) view.findViewById(R.id.button_add_friend_to_list);
        textNewGuest = (EditText) view.findViewById(R.id.text_new_guest);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String friend = textNewGuest.getText().toString().trim();
                if (friend.length() == 0) {
                    Toast.makeText(getContext(), R.string.field_empty, Toast.LENGTH_SHORT).show();
                } else if (friend.equals(login.getUsername())) {
                    Toast.makeText(getContext(), R.string.cannot_invite_yourself, Toast.LENGTH_SHORT).show();
                } else if (guests.contains(friend)) {
                    Toast.makeText(getContext(), String.format(getString(R.string.friend_already_added), friend), Toast.LENGTH_SHORT).show();
                } else {
                    adapter.add(friend);
                    recyclerView.scrollToPosition(0);
                }
                textNewGuest.setText("");
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);

        setEditable(editable);

        recyclerView.setAdapter(adapter);
    }

    public ArrayList<String> getGuests() {
        return guests;
    }

    public void setGuests(Collection<String> guests) {
        if (adapter != null) {
            adapter.clear();
            adapter.addAll(guests);
        } else {
            this.guests = new ArrayList<>(guests);
        }
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        int visibility = editable ? View.VISIBLE : View.GONE;
        if (getView() != null) {
            View containerEdit = getView().findViewById(R.id.container_edit);
            containerEdit.setVisibility(visibility);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(BUNDLE_KEY_EDITABLE, editable);
    }
}
