package st.alker.tomeet.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import st.alker.tomeet.R;
import st.alker.tomeet.activities.MainActivity;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 */

public class NotificationHelper {

    private final static String TAG = "NotificationHelper";

    public static NotificationHelper notificationHelper;

    public void notifyUser(String title, String message, Context context) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_n)
                        .setContentTitle(title)
                        .setContentText(message);

        NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        nm.notify(987654, mBuilder.build());
        Log.i(TAG, "Sent a notification");
    }

    public static void sendNotification(Context context, String title, String message, int notificationId) {

        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                intent, PendingIntent.FLAG_ONE_SHOT);

        int color = ContextCompat.getColor(context, R.color.colorPrimary);

        NotificationCompat.Builder builder = new android.support.v7.app.NotificationCompat.Builder(context);
        builder.setSmallIcon(R.drawable.ic_n)
                .setContentTitle(title)
                .setStyle(new android.support.v7.app.NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Boolean notificationsEnabled = prefs.getBoolean(context.getString(R.string.pref_key_notifications), true);
        if (!notificationsEnabled) {
            return;
        }

        Uri soundUri = null;
        String soundUriString = prefs.getString(context.getString(R.string.pref_key_notifications_ringtone), null);
        if(soundUriString == null) {
            soundUri = Settings.System.DEFAULT_NOTIFICATION_URI;
        } else if (!"".equals(soundUriString)) {
            soundUri = Uri.parse(soundUriString);
        }
        if(soundUri != null) {
            builder.setSound(soundUri);
        }


        Boolean vibrationEnabled = prefs.getBoolean(context.getString(R.string.pref_key_notifications_vibrate), true);
        if (vibrationEnabled) {
            builder.setVibrate(new long[] { 0L, 1000L});
        }

        Boolean lightsEnabled = prefs.getBoolean(context.getString(R.string.pref_key_notifications_lights), true);
        if(lightsEnabled) {
            builder.setLights(color, 1, 1);
        }

        builder.setContentIntent(contentIntent);
        notificationManager.notify(notificationId, builder.build());
    }
}
