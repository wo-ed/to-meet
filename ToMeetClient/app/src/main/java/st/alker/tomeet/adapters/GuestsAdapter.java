package st.alker.tomeet.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import st.alker.tomeet.R;

import java.util.ArrayList;

public class GuestsAdapter extends DataAdapter<String, GuestsAdapter.ViewHolder> {
    private Context context;
    private boolean editable;

    private int itemBgColor;
    private Drawable rightRounded;

    public GuestsAdapter(Context context, ArrayList<String> guests, boolean editable) {
        super(guests);
        this.context = context;
        this.editable = editable;
        itemBgColor = ContextCompat.getColor(context, R.color.colorContentBackground);
        rightRounded = ContextCompat.getDrawable(context, R.drawable.right_rounded);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_guest, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String item = items.get(position);
        holder.textView.setText(item);
        holder.buttonRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                remove(holder.getAdapterPosition());
            }
        });

        if(editable) {
            holder.buttonRemove.setVisibility(View.VISIBLE);
            holder.container.setBackground(rightRounded);
        } else {
            holder.buttonRemove.setVisibility(View.INVISIBLE);
            holder.container.setBackgroundColor(itemBgColor);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;
        private ImageButton buttonRemove;
        private LinearLayout container;

        ViewHolder(View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.text_guest_name);
            this.buttonRemove = (ImageButton) itemView.findViewById(R.id.button_remove_guest);
            this.container = (LinearLayout) itemView.findViewById(R.id.wrapper_guest);
        }

        public ImageButton getButtonRemove() {
            return buttonRemove;
        }
    }

    @Override
    public void add(String s) {
        items.add(0, s);
        notifyItemInserted(0);
    }
}
