package st.alker.tomeet.rest.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

// If needed create separate classes for get/post/put/delete (different parameters)
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

// If needed create separate classes for get/post/put/delete (different parameters)
public class Event implements Parcelable {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm", Locale.GERMANY);
    private static final String TAG = "Event";

    @SerializedName("Id")
    private int id;
    @SerializedName("HostUserName")
    private String hostUserName;
    @SerializedName("Name")
    private String name;
    @SerializedName("Description")
    private String description;
    @SerializedName("Latitude")
    private String latitude;
    @SerializedName("Longitude")
    private String longitude;
    @SerializedName("Date")
    private Date date;
    @SerializedName("Info")
    private String info;
    @SerializedName("InvitedUsers")
    private HashSet<String> invitedUsers;
    @SerializedName("InvitationId")
    private int invitationId;

    public Event() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHostUserName() {
        return hostUserName;
    }

    public void setHostUserName(String hostUserName) {
        this.hostUserName = hostUserName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public HashSet<String> getInvitedUsers() {
        return invitedUsers;
    }

    public void setInvitedUsers(HashSet<String> invitedUsers) {
        this.invitedUsers = invitedUsers;
    }

    public int getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(int invitationId) {
        this.invitationId = invitationId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(hostUserName);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(latitude);
        parcel.writeString(longitude);
        parcel.writeString(dateFormat.format(date));
        parcel.writeString(info);
        parcel.writeStringList(new ArrayList<>(invitedUsers));
        parcel.writeInt(invitationId);
    }

    protected Event(Parcel in) {
        id = in.readInt();
        hostUserName = in.readString();
        name = in.readString();
        description = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        try {
            date = dateFormat.parse(in.readString());
        } catch (ParseException e) {
            Log.e(TAG,"Date could not be parsed", e);
        }
        info = in.readString();
        List<String> stringList = new ArrayList<>();
        in.readStringList(stringList);
        invitedUsers = new HashSet<>(stringList);
        invitationId = in.readInt();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
}
