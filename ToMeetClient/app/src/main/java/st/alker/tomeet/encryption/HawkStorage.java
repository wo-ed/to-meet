package st.alker.tomeet.encryption;

import android.content.Context;
import com.orhanobut.hawk.Hawk;

public class HawkStorage implements EncryptedKeyValueStorage {

    @Override
    public <T> boolean put(String key, T value) {
        return Hawk.put(key, value);
    }

    @Override
    public <T> T get(String key) {
        return Hawk.get(key);
    }

    @Override
    public <T> T get(String key, T defaultValue) {
        return Hawk.get(key, defaultValue);
    }

    @Override
    public boolean contains(String key) {
        return Hawk.contains(key);
    }

    @Override
    public boolean delete(String key) {
        return Hawk.delete(key);
    }

    @Override
    public boolean deleteAll() {
        return Hawk.deleteAll();
    }

    @Override
    public void initialize(Context context) {
        Hawk.init(context).build();
    }
}
