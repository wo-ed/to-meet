package st.alker.tomeet.adapters;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import st.alker.tomeet.R;
import st.alker.tomeet.activities.AddEventActivity;
import st.alker.tomeet.rest.entities.Event;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class EventsAdapter extends DataAdapter<Event, EventsAdapter.ViewHolder> {
    private static final String TAG = "EventsAdapter";
    private Context context;
    private DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMANY);
    private EventsAdapter.InteractionListener listener;

    public EventsAdapter(Context context, InteractionListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EventsAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_event, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Event event = items.get(position);
        holder.textName.setText(event.getName());
        if(event.getDate() != null) {
            String date = dateFormat.format(event.getDate());
            holder.textDate.setText(String.format(context.getString(R.string.time_and_date), date));
            holder.textDate.setVisibility(View.VISIBLE);
        } else {
            holder.textDate.setVisibility(View.GONE);
        }

        if(event.getDescription() == null || event.getDescription().equals("")) {
            holder.textDescription.setText(R.string.no_description);
        } else {
            holder.textDescription.setText(event.getDescription());
        }


        double lat = 0.0;
        double lng = 0.0;
        try {
            lat = Location.convert(event.getLatitude());
        } catch (Exception e) {
            Log.e(TAG, String.format("Converting latitude of Event %s failed", event.getId()), e);
        }
        try {
            lng = Location.convert(event.getLongitude());
        } catch (Exception e) {
            Log.e(TAG, String.format("Converting latitude of Event %s failed", event.getId()), e);
        }
        holder.textLocation.setText(String.format(context.getString(R.string.location), lat, lng));

        holder.textEventHost.setText(String.format(context.getString(R.string.host), event.getHostUserName()));

        holder.buttonEventLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.showEventOnMap(event);
            }
        });

        /*
        StringBuilder builder = new StringBuilder();
        for(String user : event.getInvitedUsers()) {
            builder.append(user);
            builder.append(", ");
        }
        holder.textInvitedUsers.setText(String.format(context.getString(R.string.invited_users), builder.toString()));
        holder.textInvitedUsers.setText(String.format(context.getString(R.string.invited_users), builder.toString()));
        */

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AddEventActivity.class);
                intent.putExtra(AddEventActivity.INTENT_KEY_EVENT, event);
                context.startActivity(intent);
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textName;
        private TextView textDescription;
        private TextView textDate;
        private TextView textLocation;
        private ImageButton buttonEventLocation;
        private TextView textEventHost;

        ViewHolder(View view) {
            super(view);
            textName = (TextView) view.findViewById(R.id.text_event_name);
            textDescription = (TextView) view.findViewById(R.id.text_event_description);
            textDate = (TextView) view.findViewById(R.id.text_event_date);
            textLocation = (TextView) view.findViewById(R.id.text_event_location);
            buttonEventLocation = (ImageButton) view.findViewById(R.id.button_event_location);
            textEventHost = (TextView) view.findViewById(R.id.text_event_host);
        }
    }

    public interface InteractionListener {
        void showEventOnMap(Event event);
    }
}
