package st.alker.tomeet.geofence;

import android.Manifest;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import cz.msebera.android.httpclient.Header;
import st.alker.tomeet.Globals;
import st.alker.tomeet.R;
import st.alker.tomeet.alarm.AlarmService;
import st.alker.tomeet.rest.Constants;
import st.alker.tomeet.rest.GsonHttpResponseHandler;
import st.alker.tomeet.rest.RestHelper;
import st.alker.tomeet.rest.entities.Event;
import st.alker.tomeet.rest.entities.LoginResponse;
import st.alker.tomeet.utils.LocationReceiver;

public class GeoService extends IntentService implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {

    public static final Object eventTriggerAlarmWait = new Object();
    private static final String TAG = "GeoService";
    private static final Object eventWait = new Object();
    private static GoogleApiClient mGoogleApiClient; // the GoogleAPIClient used to connect to the OS
    private ArrayList<Geofence> mGeofenceList; // list containing all current Geofences
    private RestHelper restHelper;
    private GsonHttpResponseHandler<Event[]> eventHandler = new GsonHttpResponseHandler<Event[]>(Event[].class, Globals.GSON) {

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "Failure on getting events from server with String: " + responseString + " and status code: " + statusCode);
        }


        @Override
        public void onSuccess(int statusCode, Header[] headers, Event[] events) {
            Log.i(TAG, "Success on getting events");
            updateEventLocations(events);
        }

        @Override
        public void onSuccessParsingFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "Success, but parsing Failure on getting event from Server");
        }

    };

    public GeoService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(final Intent intent) {

        String type = intent.getStringExtra("type");
        if (type == null) {
            return;
        }

        switch (type) {
            case "disableGeofences":
                disableGeofences();
                break;
            case "geoBoot":
                addGeofencesAfterReboot();
                break;
            case "getEvents":
                new Thread() {
                    public void run() {
                        restHelper.getObjectsAuthorized(Constants.Urls.EVENTS, eventHandler);
                    }
                }.start();
                synchronized (eventWait) {
                    try {
                        eventWait.wait(30000);
                        if (!mGoogleApiClient.isConnected()) {
                            return;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case "removeGeofence":
                String id = intent.getStringExtra("ID");
                removeGeoFence(id);
                break;
            case "resetGeofence":
                int eventID = intent.getIntExtra("event", -1);
                if (eventID == -1) return;
                resetGeofence(eventID);
                break;
        }

    }

    private void resetGeofence(int eventID) {
        String eventLocations;
        synchronized (LocationReceiver.eventSynchronizer) {
            SharedPreferences prefs = getSharedPreferences("eventLocationData", MODE_PRIVATE);
            eventLocations = prefs.getString("eventLocations", "Abort...");
        }
        Event currentEvent = null;
        if (eventLocations.equals("Abort...")) {
            Log.e(TAG, "Aborted...eventLocations was not in sharedPrefs/we don't have any events yet");
            return;
        } else {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<Event>>() {
            }.getType();
            ArrayList<Event> eventLocationList = gson.fromJson(eventLocations, type);
            for (Event event : eventLocationList) {
                if (Integer.toString(eventID).equals(Integer.toString(event.getId()))) {
                    currentEvent = event;
                    break;
                }
            }
            if (currentEvent == null) return;
        }
        Log.e(TAG, "RESETTING GEOFENCE: " + currentEvent.getName());
        removeGeoFence(Integer.toString(currentEvent.getId()));
        addToGeofenceList(Integer.toString(eventID), Double.parseDouble(currentEvent.getLatitude()), Double.parseDouble(currentEvent.getLongitude()), calculateExpirationTime(currentEvent.getDate()));
        registerGeofences();

    }

    private void checkForTriggerAlarm(Event event) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        synchronized (eventTriggerAlarmWait) {
            SharedPreferences prefs = getSharedPreferences("eventTriggerAlarm", MODE_PRIVATE);
            String addedEvents = prefs.getString("eventTriggerAlarm", "Abort...");
            ArrayList<Event> addedEventsList;
            Gson gson = new Gson();
            if (addedEvents.equals("Abort...")) {
                addedEventsList = new ArrayList<>();
            } else {
                Type type = new TypeToken<ArrayList<Event>>() {
                }.getType();
                addedEventsList = gson.fromJson(addedEvents, type);
            }
            for (Event addedEvent : addedEventsList) {
                if (Integer.toString(addedEvent.getId()).equals(Integer.toString(event.getId()))) {
                    return;
                }
            }
            addedEventsList.add(event);
            Log.e(TAG, "added event: " + event.getName() + " to ignore list for future alarms");
            SharedPreferences.Editor editor = prefs.edit();
            String input = gson.toJson(addedEventsList);
            editor.remove("eventTriggerAlarm");
            editor.commit();
            editor.putString("eventTriggerAlarm", input);
            editor.apply();
        }
        AlarmService.scheduleEventAlarm(event, this);
    }


    private void updateEventLocations(Event[] events) {

        Intent container = new Intent();
        String eventLocations;
        synchronized (LocationReceiver.eventSynchronizer) {
            SharedPreferences prefs = getSharedPreferences("eventLocationData", MODE_PRIVATE);
            eventLocations = prefs.getString("eventLocations", "Abort...");
        }
        if (eventLocations.equals("Abort...")) {
            Log.w(TAG, "Aborted...eventLocations was not in sharedPrefs/we don't have any events yet");
        } else {
            disableGeofences();
            synchronized (mGeofenceList) {
                mGeofenceList.clear();
            }
        }

        ArrayList<Event> eventList = new ArrayList<>();
        for (Event event : events) {
            eventList.add(event);
            int expirationTime = calculateExpirationTime(event.getDate());
            if (expirationTime == 0) continue;
            addGeoFence(Integer.toString(event.getId()), Double.parseDouble(event.getLatitude()), Double.parseDouble(event.getLongitude()), expirationTime);
            checkForTriggerAlarm(event);
        }

        registerGeofences();

        container.putExtra("type", "events");
        container.putExtra("eventList", eventList);

        this.sendBroadcast(container.setAction("updateMapFragment"));

        synchronized (eventWait) {
            eventWait.notifyAll();
        }
    }

    private int calculateExpirationTime(Date eventDate) {

        Date date = new Date(); //currentDate
        long deltaMiliseconds = eventDate.getTime() - date.getTime(); //delta between now and event
        long deltaSeconds = deltaMiliseconds / 1000;
        long deltaMinutes = deltaSeconds / 60;
        long deltaHours = deltaMinutes / 60;

        int expirationTime = (int) deltaHours;

        /**allow +60 minutes in case of a delay, so that the geofence doesn't expire as soon as the event starts*/
        if (deltaHours > -60) {
            return expirationTime + 61; //adjust expiration time, so that the geofence doesn't expire as soon as the event starts
        }
        return 0; //return 0, so that the expirationTime can't be negative
    }


    /**
     * Working with geofences
     */

    protected void addGeoFence(String eventID, double latitude, double longitude, int expirationTime) {
        if (!isLoggedIn()) {
            Log.i(TAG, "User is not logged in");
            return;
        }
        addToGeofenceList(eventID, latitude, longitude, expirationTime);
    }

    protected void removeGeoFence(String eventID) {
        unregisterGeofences();
        removeFromGeofenceList(eventID);
        registerGeofences();
    }

    protected void disableGeofences() {
        unregisterGeofences();
    }

    /**
     * Registers the geofences to the OS
     */
    private void registerGeofences() {
        try {
            if (!mGoogleApiClient.isConnected()) {
                Log.i(TAG, "GoogleAPIClient is not connected");
                return;
            }
            synchronized (mGeofenceList) {

                if (!(mGeofenceList.size() > 0)) {
                    //we don't have any geofences to register
                    return;
                }
            }
            LocationServices.GeofencingApi.addGeofences(
                    mGoogleApiClient,
                    getGeofencingRequest(),
                    getGeofencePendingIntent()
            ).setResultCallback(this); // Result processed in onResult().
            Log.i(TAG, "registered Geofences");
        } catch (SecurityException securityException) {
            Log.e(TAG, securityException.toString());
        }
    }

    /**
     * Unregisters the geofences from the OS
     */
    private void unregisterGeofences() {
        try {
            if (!mGoogleApiClient.isConnected()) {
                Log.i(TAG, "GoogleAPIClient was not connected.");
                return;
            }
            LocationServices.GeofencingApi.removeGeofences(
                    mGoogleApiClient,
                    getGeofencePendingIntent()
            ).setResultCallback(this); // Result processed in onResult().
        } catch (SecurityException securityException) {
            Log.e(TAG, securityException.toString());
        } catch (IllegalStateException e) {
            Log.e(TAG, "GoogleAPIClient is not connected");
        }
    }

    /**
     * adds a geofence to the list
     *
     * @param eventID        Unique identifier of the event.
     * @param latitude       latitude of the event marker
     * @param longitude      longitude of the event marker
     * @param expirationTime in hours
     */
    private void addToGeofenceList(String eventID, double latitude, double longitude, int expirationTime) {
        int radius = 150;
        synchronized (mGeofenceList) {

            mGeofenceList.add(new Geofence.Builder()
                    .setRequestId(eventID)
                    .setCircularRegion(
                            latitude,
                            longitude,
                            radius
                    )
                    .setExpirationDuration(expirationTime * 1000 * 60 * 60)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                    .build());
        }
    }

    /**
     * removes a geofence from the list
     *
     * @param eventID ID of the event to remove
     */
    private void removeFromGeofenceList(String eventID) {
        synchronized (mGeofenceList) {
            Iterator<Geofence> i = mGeofenceList.iterator();
            while (i.hasNext()) {
                Geofence geofence = i.next();
                if (geofence.getRequestId().equals(eventID)) {
                    i.remove();
                }
            }
        }
    }


    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     * We can always re-use the same list, according to the documentation if the same request ID for a geofence
     * is used, the new one will replace the old one.
     * https://developers.google.com/android/reference/com/google/android/gms/location/GeofencingApi
     */
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        /* The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
         * GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
         * is already inside that geofence.
         */
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        synchronized (mGeofenceList) {
            builder.addGeofences(mGeofenceList);
        }
        return builder.build();
    }

    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */
    private PendingIntent getGeofencePendingIntent() {
        Intent intent = new Intent(this, GeoReceiver.class);
        return PendingIntent.getBroadcast(this, 654789, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    /**
     * setResultCallback()-Method after adding/removing geofences
     */
    public void onResult(@NonNull Status status) {
        if (status.isSuccess()) {
            Log.i(TAG, "Geofences have been added/removed");

        } else {
            Log.w(TAG, Integer.toString(status.getStatusCode()));
            if (status.getStatusCode() == 1000) {
                Log.w(TAG, "A higher location mode (Settings-Location-Mode) needs to be enabled");
                Log.w(TAG, "This might also have a different cause");
            }
        }
    }

    /**
     * Geofences are not persistent after a reboot
     * This method readds all available geofences
     */
    protected void addGeofencesAfterReboot() {

        if (!isLoggedIn()) {
            Log.i(TAG, "User is not logged in");
            return;
        }
        String eventLocations;
        synchronized (LocationReceiver.eventSynchronizer) {
            SharedPreferences prefs = getSharedPreferences("eventLocationData", MODE_PRIVATE);
            eventLocations = prefs.getString("eventLocations", "Abort...");
        }
        if (eventLocations.equals("Abort...")) {
            Log.i(TAG, "Aborted...eventLocations was not in sharedPrefs/we don't have any events yet");
        } else {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<Event>>() {
            }.getType();
            ArrayList<Event> eventLocationList = gson.fromJson(eventLocations, type);

            for (Event event : eventLocationList) {
                int expirationTime = calculateExpirationTime(event.getDate());
                if (expirationTime == 0) continue;
                addToGeofenceList(Integer.toString(event.getId()), Double.parseDouble(event.getLatitude()), Double.parseDouble(event.getLongitude()), expirationTime);
            }
        }
        registerGeofences();
    }


    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "Connected to GoogleApiClient");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.e(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.w(TAG, "Connection to GoogleApiClient suspended");
    }

    /**
     * Method to check if the user is currently logged in
     *
     * @return whether the user is logged in
     */
    private boolean isLoggedIn() {
        return Globals.ENCRYPTED_STORAGE.contains(getString(R.string.storage_key_login_data));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (mGeofenceList == null) mGeofenceList = new ArrayList<Geofence>();
        try {
            LoginResponse login = Globals.ENCRYPTED_STORAGE.get(getString(R.string.storage_key_login_data));
            restHelper = new RestHelper(this, login);
        } catch (NullPointerException e) {
            //Nullpointer on logout, login credentials are no longer available
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
        Log.i(TAG, "GoogleApiClient was disconnected");
    }
}
