package st.alker.tomeet.encryption;

import android.content.Context;

public interface EncryptedKeyValueStorage {
    <T> boolean put(String key, T value);
    <T> T get(String key);
    <T> T get(String key, T defaultValue);
    boolean contains(String key);
    boolean delete(String key);
    boolean deleteAll();
    void initialize(Context context);
}
