package st.alker.tomeet.alarm;

import android.Manifest;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import st.alker.tomeet.Globals;
import st.alker.tomeet.R;
import st.alker.tomeet.geofence.GeoService;
import st.alker.tomeet.rest.Constants;
import st.alker.tomeet.rest.GsonHttpResponseHandler;
import st.alker.tomeet.rest.RestHelper;
import st.alker.tomeet.rest.entities.Device;
import st.alker.tomeet.rest.entities.Event;
import st.alker.tomeet.rest.entities.LoginResponse;
import st.alker.tomeet.rest.entities.TrackingPermission;
import st.alker.tomeet.utils.BluetoothUtil;
import st.alker.tomeet.utils.LocationReceiver;

/**
 * Service that receives the alarm intents
 */
public class AlarmService extends IntentService implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final int STANDARD_INTERVAL_CODE = 953751;
    public static final int EVENT_INTERVAL_CODE = 157359;
    private static final String TAG = "AlarmService";
    private final Object sendWait = new Object();
    private final Object getWait = new Object();
    private GoogleApiClient mGoogleApiClient;
    private LocationManager lm;
    private String bluetoothMACAddress;
    /**
     * Handler to send device to server
     */
    private GsonHttpResponseHandler<Device> sendDeviceHandler = new GsonHttpResponseHandler<Device>(Device.class, Globals.GSON) {

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "Failure on sending device to server with String: " + responseString + "and status code: " + statusCode);
            synchronized (sendWait) {
                sendWait.notifyAll();
            }
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, Device device) {
            Log.i(TAG, "Success on sending to server with Device device");
            synchronized (sendWait) {
                sendWait.notifyAll();
            }
        }

        @Override
        public void onSuccessParsingFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "Success, but parsing Failure (sending to server)");
            synchronized (sendWait) {
                sendWait.notifyAll();
            }
        }

    };

    /**
     * Handler to send our own location to server
     */
    private GsonHttpResponseHandler<TrackingPermission[]> getDevicesHandler = new GsonHttpResponseHandler<TrackingPermission[]>(TrackingPermission[].class, Globals.GSON) {

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "Failure on getting devices from server with String: " + responseString + " and statuscode: " + statusCode);
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, TrackingPermission[] trackingPermissions) {
            Log.i(TAG, "Success on getting trackingpermissions");
            updateFriendLocations(trackingPermissions);
        }

        @Override
        public void onSuccessParsingFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "Success, but parsing Failure on getting devices from Server");
        }

    };

    public AlarmService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences sharedPrefs = getApplicationContext().getSharedPreferences("alarmPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean("alarms_recently_started", false);
        editor.apply();

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        bluetoothMACAddress = BluetoothUtil.getBluetoothMacAddress(getContentResolver());
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String type = intent.getStringExtra("type");

        if (type == null) {
            return;
        }

        switch (type) {
            case "alarm":
                WakefulBroadcastReceiver.completeWakefulIntent(intent);
                Log.i(TAG, "ALARM SOUNDED: " + new SimpleDateFormat("dd.MM.yyyy.hh.mm.ss").format(new Date()));

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    cancelAlarm(); //cancel all other alarms
                    return;
                }

                new Thread() {
                    public void run() {
                        Looper.prepare();
                        mGoogleApiClient.connect();
                    }
                }.start();

                synchronized (sendWait) {
                    try {
                        sendWait.wait(30000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                /**
                 *  switch from EventAlarm -> StandardAlarm
                 */
                int rc = intent.getIntExtra("RequestCode", STANDARD_INTERVAL_CODE);
        /* RequestCode equals event code and getNextIntervalCode no longer returns the event interval code = switch to normal alarm needed*/
                if (rc == EVENT_INTERVAL_CODE && !(rc == getNextAlarmCode())) {
                    scheduleAlarm(getNextAlarmCode());
                }

                /**
                 * Get position of events
                 */
                Intent geoIntent = new Intent(this, GeoService.class);
                geoIntent.putExtra("type", "getEvents");
                startService(geoIntent);
                break;

            /**no break needed, we also want to get friend locations*/
            case "getFriends":
                /**
                 * Get position of friends/devices
                 */

                LoginResponse login = Globals.ENCRYPTED_STORAGE.get(getString(R.string.storage_key_login_data), null);
                if(login != null) {
                    RestHelper restHelper = new RestHelper(this, login);
                    restHelper.getObjectsAuthorized(Constants.Urls.TRACKING_PERMISSIONS, getDevicesHandler);
                }

                synchronized (getWait) {
                    try {
                        getWait.wait(30000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            case "bootAlarm":
                startUpAlarmManagement();
                break;
            case "cancelAlarms":
                cancelAlarm();
                break;
        }
    }


    private void updateFriendLocations(TrackingPermission[] trackingPermissions) {
        Intent container = new Intent();
        ArrayList<Device> friends = new ArrayList<>();
        for (TrackingPermission permission : trackingPermissions) {
            if (!isUserDeviceOwner(permission)) {
                for (TrackingPermission otherPermission : trackingPermissions) {
                    if (isUserDeviceOwner(otherPermission)
                            && otherPermission.getDevice().getMacBluetooth().equals(bluetoothMACAddress)
                            && otherPermission.getUserName().equals(permission.getDevice().getUserName())) {
                        friends.add(permission.getDevice());
                        break;
                    }
                }
            }
        }

        container.putExtra("friendList", friends);
        container.putExtra("type", "friends");

        this.sendBroadcast(container.setAction("updateMapFragment"));

        synchronized (getWait) {
            getWait.notifyAll();
        }
    }

    public static void scheduleEventAlarm(Event event, Context context) {
        Intent intent = new Intent(context.getApplicationContext(), AlarmReceiver.class);
        intent.putExtra("type", "resetGeofence");
        intent.putExtra("event", event.getId());
        if (event.getDate().getTime() - System.currentTimeMillis() + 60*60*1000 < 0) return;
        final PendingIntent pendingIntent = PendingIntent.getBroadcast(context, event.getId(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, event.getDate().getTime()-55*60*1000, pendingIntent);
    }

    /**
     * Working with alarms
     */

    /**
     * Schedules a new alarm
     *
     * @param INTERVAL_CODE alarm type used - standard/event
     */
    protected void scheduleAlarm(final int INTERVAL_CODE) {
        SharedPreferences sharedPrefs = getSharedPreferences("alarmPrefs", MODE_PRIVATE);
        if (isLoggedIn()) {
            if (!sharedPrefs.getBoolean("alarms_recently_started", false)) {

                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putBoolean("alarms_recently_started", true);
                editor.apply();

                rsaSwitch();

                Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
                intent.putExtra("RequestCode", getNextAlarmCode());
                intent.putExtra("type", "alarm");
                final PendingIntent pendingIntent = PendingIntent.getBroadcast(this, INTERVAL_CODE, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                SharedPreferences defaultPrefs = PreferenceManager.getDefaultSharedPreferences(this);
                long frequency;
                if (INTERVAL_CODE == STANDARD_INTERVAL_CODE) {
                    frequency = Integer.parseInt(defaultPrefs.getString("sync_frequency", "60")) * 1000 * 60; //*1000 to get to seconds, *60 to get to minutes
                } else {
                    frequency = 5 * 1000 * 60; // 5 minute alarm before events
                }
                alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 5000, frequency, pendingIntent); //5000 is the minimum value Android accepts
                Log.i(TAG, "Alarm Started");
            } else {
                Log.e(TAG, "An alarm was started within the last 10 seconds. Due to a bug in the android switch-preference, we need to have a cooldown after starting an alarm. Please try again.");
                Log.e(TAG, "The message may be ignored if triggered after using a switch-preference");

            }
        } else {
            Log.e(TAG, "User is not logged in");
        }
    }

    /**
     * Method to cancel all alarms
     */
    protected void cancelAlarm() {
        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        final PendingIntent standardIntent = PendingIntent.getBroadcast(this, STANDARD_INTERVAL_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        final PendingIntent eventIntent = PendingIntent.getBroadcast(this, EVENT_INTERVAL_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(standardIntent);
        alarm.cancel(eventIntent);
        Log.i(TAG, "Alarms stopped");
    }

    /**
     * Method to start an alarm after the application has been launched
     */
    protected void startUpAlarmManagement() {
        //needed to load default preferences to start alarm
        //the third parameter - readAgain/false - makes sure user preferences are not overwritten.
        PreferenceManager.setDefaultValues(this, R.xml.pref_data_sync, false);
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (isLoggedIn()) {
            if (!sharedPrefs.getBoolean("offline_mode", true)) { //user is not in offline mode
                if (sharedPrefs.getBoolean("get_tracked", false)) { //user wants to send their location data to the server
                    scheduleAlarm(getNextAlarmCode()); //starts the alarm with the specified Interval Code.
                } else {
                    cancelAlarm();
                }
            } else {
                cancelAlarm();
            }
        } else {
            Log.e(TAG, "User is not logged in");
            cancelAlarm();
        }
    }

    protected void updateLocation() {
        if (lm == null) {
            lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            cancelAlarm();
            return;
        }

        if (!mGoogleApiClient.isConnected()) {
            Log.e(TAG, "GoogleApiClient not connected. Operation aborted.");
            return;
        }

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            Log.i(TAG, "FusedLocationAPI-location available, sending to server");
            getLocationValues(mLastLocation);
        } else {
            Location location;
            if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                Log.i(TAG, "GPS Provider");
            } else if (lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                Log.i(TAG, "Network Provider");
            } else {
                location = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                Log.i(TAG, "Passive Provider");
            }
            if (location == null) {
                Log.w(TAG, "No location available, requesting new");

                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
                lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
                lm.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 1000, 0, this);
            } else {
                Log.i(TAG, "normal location available, sending to server");
                getLocationValues(location);
            }
        }
    }

    /**
     * Server requires a device - Bluetooth-MAC, longitude, latitude, last Date
     *
     * @param latitude  latitude of the current location
     * @param longitude longitude of the current location
     */
    private void connectToServer(final Double latitude, final Double longitude) {
        LoginResponse login = Globals.ENCRYPTED_STORAGE.get(getString(R.string.storage_key_login_data), null);
        if(login == null) {
            return;
        }

        Device device = new Device();
        device.setLastDate(new Date());
        device.setUserName(login.getUsername());
        device.setLastLatitude(Double.toString(latitude));
        device.setLastLongitude(Double.toString(longitude));
        String localMacAddress = bluetoothMACAddress;
        device.setMacBluetooth(localMacAddress);

        String fcmToken = Globals.ENCRYPTED_STORAGE.get(getString(R.string.storage_key_fcm_token), null);
        device.setFcmToken(fcmToken);

        RestHelper restHelper = new RestHelper(this, login);
        restHelper.postObjectAuthorized(Constants.Urls.DEVICES, device, sendDeviceHandler);
    }

    private void getLocationValues(Location location) {
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();
        connectToServer(latitude, longitude);
    }


    /**
     * Method to check if the user is currently logged in
     *
     * @return whether the user is logged in
     */
    private boolean isLoggedIn() {
        return Globals.ENCRYPTED_STORAGE.contains(getString(R.string.storage_key_login_data));
    }


    /**
     * Method to check whether an event is happening soon (1h).
     * If yes, the alarm event code should be used
     *
     * @return the correct Interval code
     */
    protected int getNextAlarmCode() {
        String eventLocations;
        synchronized (LocationReceiver.eventSynchronizer) {
            SharedPreferences prefs = getSharedPreferences("eventLocationData", MODE_PRIVATE);
            eventLocations = prefs.getString("eventLocations", "Abort...");
        }

        if (eventLocations.equals("Abort...")) {
            Log.e(TAG, "Aborted...eventLocations was not in sharedPrefs/we don't have any events yet");
        } else {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<Event>>() {
            }.getType();
            ArrayList<Event> eventLocationList = gson.fromJson(eventLocations, type);
            for (Event event : eventLocationList) {
                Date date = new Date(); //currentDate
                long deltaMiliseconds = event.getDate().getTime() - date.getTime(); //delta between now and event
                long deltaSeconds = deltaMiliseconds / 1000;
                long deltaMinutes = deltaSeconds / 60;
                if (deltaMinutes >= 0 && deltaMinutes <= 60) {
                    return EVENT_INTERVAL_CODE;
                }
            }
        }
        return STANDARD_INTERVAL_CODE;
    }

    /**
     * Recently-started-alams switch method
     * Needed because of a bug in the switch-preference implementation
     */
    private void rsaSwitch() {
        new Thread() {
            public void run() {
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                SharedPreferences sharedPrefs = getSharedPreferences("alarmPrefs", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putBoolean("alarms_recently_started", false);
                editor.apply();
            }
        }.start();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "Location changed, sending to server");
        getLocationValues(location);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "Permission Error");
            return;
        }
        lm.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "GoogleApiClient connected");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            cancelAlarm();
            return;
        }
        LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient); //request new location;
        updateLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "GoogleApiClients connection was suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "GoogleApiClients connection failed");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }

    private boolean isUserDeviceOwner(TrackingPermission permission) {
        LoginResponse login = Globals.ENCRYPTED_STORAGE.get(getString(R.string.storage_key_login_data), null);
        if(login == null) {
            return false;
        }
        return login.getUsername().equals(permission.getDevice().getUserName());
    }
}
