package st.alker.tomeet.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;

import st.alker.tomeet.Globals;
import st.alker.tomeet.R;
import st.alker.tomeet.adapters.EventsAdapter;
import st.alker.tomeet.adapters.FriendsAdapter;
import st.alker.tomeet.alarm.AlarmService;
import st.alker.tomeet.firebase.MyFirebaseInstanceIDService;
import st.alker.tomeet.fragments.BluetoothFragment;
import st.alker.tomeet.fragments.EventsFragment;
import st.alker.tomeet.fragments.FriendsFragment;
import st.alker.tomeet.fragments.TrackingFragment;
import st.alker.tomeet.geofence.GeoService;
import st.alker.tomeet.rest.entities.Device;
import st.alker.tomeet.rest.entities.Event;
import st.alker.tomeet.rest.entities.LoginResponse;
import st.alker.tomeet.utils.LocationReceiver;

public class MainActivity extends AppCompatActivity implements FriendsAdapter.InteractionListener,
        EventsAdapter.InteractionListener, NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "MainActivity";
    private static final String BUNDLE_KEY_TITLE = "TITLE";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private int selectedItemIndex = 0;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LoginResponse login = Globals.ENCRYPTED_STORAGE.get(getString(R.string.storage_key_login_data));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            // Initialize the start page
            chooseContentByMenuItem(navigationView.getMenu().findItem(R.id.nav_map));
        } else {
            // The title is set by the selected menu item, that's why we need to restore it
            setTitle(savedInstanceState.getString(BUNDLE_KEY_TITLE));
        }

        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.label_username)).setText(login.getUsername());

        if (checkPlayServices()) {
            startService(new Intent(this, MyFirebaseInstanceIDService.class));
            String token = FirebaseInstanceId.getInstance().getToken();
            Log.d(TAG, "token: " + token);
            if(token != null && !token.equals("")) {
                Globals.ENCRYPTED_STORAGE.put(getString(R.string.storage_key_fcm_token), token);
            }
        }
        /**Update */
        Intent alarmIntent = new Intent(this, AlarmService.class);
        alarmIntent.putExtra("type", "getFriends");
        startService(alarmIntent);
        Intent geoIntent = new Intent(this, GeoService.class);
        geoIntent.putExtra("type", "getEvents");
        startService(geoIntent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        if (item.getItemId() == R.id.nav_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
        } else if (item.getItemId() == R.id.nav_log_out) {
            logOut();
        } else {
            chooseContentByMenuItem(item);
        }
        return true;
    }

    private void logOut() {
        Intent alarmIntent = new Intent(this, AlarmService.class);
        alarmIntent.putExtra("type", "cancelAlarms");
        startService(alarmIntent);

        Intent geoIntent = new Intent(this, GeoService.class);
        geoIntent.putExtra("type", "disableGeofences");
        startService(geoIntent);

        /*Remove friends and events from local storage*/
        synchronized (LocationReceiver.eventSynchronizer) {
            SharedPreferences sharedPrefs = getSharedPreferences("eventLocationData", MODE_PRIVATE);
            SharedPreferences.Editor eventEditor = sharedPrefs.edit();
            eventEditor.remove("eventLocations");
            eventEditor.commit();
            eventEditor.apply();
        }
        synchronized (LocationReceiver.friendSynchronizer) {
            SharedPreferences prefs = getSharedPreferences("friendLocationData", MODE_PRIVATE);
            SharedPreferences.Editor friendEditor = prefs.edit();
            friendEditor.remove("friendLocations"); //remove old Locations
            friendEditor.commit();
            friendEditor.apply();
        }

        synchronized (GeoService.eventTriggerAlarmWait) {
            SharedPreferences prefs = getSharedPreferences("eventTriggerAlarm", MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.clear().commit();
            editor.apply();
        }

        Globals.ENCRYPTED_STORAGE.delete(getString(R.string.storage_key_login_data));
        startActivity(new Intent(this, LoginActivity.class));


        finish(); //prevent going back
    }

    private Fragment chooseContentByMenuItem(MenuItem item) {
        int id = item.getItemId();
        Class fragmentClass = null;

        switch (id) {
            case R.id.nav_map:
                fragmentClass = TrackingFragment.class;
                break;
            /*case R.id.nav_bluetooth:
                fragmentClass = BluetoothFragment.class;
                break;*/
            case R.id.nav_friends:
                fragmentClass = FriendsFragment.class;
                break;
            case R.id.nav_events:
                fragmentClass = EventsFragment.class;
                break;
            default:
                fragmentClass = TrackingFragment.class;
        }

        Fragment fragment = replaceMainContentFragment(fragmentClass, false);
        selectMenuItem(item);
        return fragment;
    }

    private void selectMenuItem(MenuItem item) {
        item.setChecked(true);
        setTitle(item.getTitle());
        selectedItemIndex = item.getItemId();
    }

    private <F extends Fragment> F replaceMainContentFragment(Class<F> fragmentClass, boolean addToBackSTack) {
        F fragment = null;
        try {
            fragment = fragmentClass.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        replaceMainContentFragment(fragment, addToBackSTack);
        return fragment;
    }

    private void replaceMainContentFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.placeholder_main, fragment);
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
                android.R.anim.fade_in, android.R.anim.fade_out);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(BUNDLE_KEY_TITLE, getTitle().toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.setCheckedItem(selectedItemIndex);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                Dialog dialog = apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
                dialog.show();
            } else {
                String message = "This device is not supported by Google Play Services.";
                Log.i(TAG, message);
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onStop() {
        SharedPreferences sharedPrefs = getSharedPreferences("alarmPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean("alarms_recently_started", false);
        editor.apply();
        super.onStop();
    }

    public void showEventOnMap(Event event) {
        TrackingFragment fragment = replaceMainContentFragment(TrackingFragment.class, false);
        selectMenuItem(navigationView.getMenu().findItem(R.id.nav_map));
        fragment.setEvent(event);
    }

    @Override
    public void showDeviceOnMap(Device device) {
        TrackingFragment fragment = replaceMainContentFragment(TrackingFragment.class, false);
        selectMenuItem(navigationView.getMenu().findItem(R.id.nav_map));
        fragment.setDevice(device);
    }

    @Override
    public void showDeviceBluetooth(Device device) {
        BluetoothFragment fragment = new BluetoothFragment();
        fragment.setMacAddress(device.getMacBluetooth());
        replaceMainContentFragment(fragment, true);
        //selectMenuItem(navigationView.getMenu().findItem(R.id.nav_bluetooth));
    }
}
