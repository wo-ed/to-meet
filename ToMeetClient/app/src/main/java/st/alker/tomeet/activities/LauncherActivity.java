package st.alker.tomeet.activities;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import st.alker.tomeet.Globals;
import st.alker.tomeet.R;
import st.alker.tomeet.alarm.AlarmService;
import st.alker.tomeet.rest.entities.LoginResponse;

import java.text.ParseException;
import java.util.Calendar;

public class LauncherActivity extends AppCompatActivity {
    private static final String TAG = "LauncherActivity";

    private static final int REQUEST_DISCOVERABLE = 0;

    private LoginResponse login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setElevation(0);
        }

        login = Globals.ENCRYPTED_STORAGE.get(getString(R.string.storage_key_login_data));

        requestDiscoverable();
        Intent alarmIntent = new Intent(this, AlarmService.class);
        alarmIntent.putExtra("type", "bootAlarm");
        startService(alarmIntent);
    }

    private boolean isUserLoggedIn() {
        if (login == null || !login.isValid()) {
            return false;
        }

        Calendar now = Calendar.getInstance();
        Calendar expirationDate = Calendar.getInstance(Globals.TIMEZONE_SERVER, Globals.LOCALE_SERVER);
        try {
            expirationDate.setTime(login.getExpirationDate());
        } catch (ParseException e) {
            Log.e(TAG, "expiration date could not be parsed", e);
            return false;
        }

        return now.before(expirationDate);
    }

    private void requestDiscoverable() {
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
        startActivityForResult(discoverableIntent, REQUEST_DISCOVERABLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_DISCOVERABLE) {
            if (!isUserLoggedIn()) {
                startActivity(new Intent(this, LoginActivity.class));
            } else {
                startActivity(new Intent(this, MainActivity.class));
            }
            finish();
        }
    }
}
