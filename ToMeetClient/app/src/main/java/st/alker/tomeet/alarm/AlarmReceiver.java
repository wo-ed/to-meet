package st.alker.tomeet.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import st.alker.tomeet.geofence.GeoService;

public class AlarmReceiver extends BroadcastReceiver {

    /**
     * Receives the PendingIntent after an alarm has been triggered
     * @param context context
     * @param intent intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        String type = intent.getStringExtra("type");

        if (type.equals("resetGeofence")) {
            Intent i = new Intent(context, GeoService.class);
            i.putExtra("type", type);
            i.putExtra("event", intent.getIntExtra("event",-1));
            context.startService(i);
        } else {
            Intent i = new Intent(context, AlarmService.class);
            i.putExtra("RequestCode", intent.getIntExtra("RequestCode", AlarmService.STANDARD_INTERVAL_CODE));
            i.putExtra("type", type);
            context.startService(i);
        }
    }
}
