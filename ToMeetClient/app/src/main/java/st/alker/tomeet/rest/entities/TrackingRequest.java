package st.alker.tomeet.rest.entities;

import com.google.gson.annotations.SerializedName;

// If needed create separate classes for get/post/put/delete (different parameters)
public class TrackingRequest {
    @SerializedName("Id")
    private int id;
    @SerializedName("DeviceId")
    private int deviceId;
    @SerializedName("TrackGPS")
    private boolean trackGps;
    @SerializedName("TrackBluetooth")
    private boolean trackBluetooth;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public boolean isTrackGps() {
        return trackGps;
    }

    public void setTrackGps(boolean trackGps) {
        this.trackGps = trackGps;
    }

    public boolean isTrackBluetooth() {
        return trackBluetooth;
    }

    public void setTrackBluetooth(boolean trackBluetooth) {
        this.trackBluetooth = trackBluetooth;
    }
}
