package st.alker.tomeet.activities;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.TextHttpResponseHandler;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.cache.Resource;
import st.alker.tomeet.Globals;
import st.alker.tomeet.R;
import st.alker.tomeet.fragments.GuestsFragment;
import st.alker.tomeet.geofence.GeoService;
import st.alker.tomeet.rest.Constants;
import st.alker.tomeet.rest.GsonHttpResponseHandler;
import st.alker.tomeet.rest.RestHelper;
import st.alker.tomeet.rest.entities.Event;
import st.alker.tomeet.rest.entities.LoginResponse;

import java.lang.ref.WeakReference;
import java.net.PortUnreachableException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

public class AddEventActivity extends AppCompatActivity {
    public static final String INTENT_KEY_EVENT = "EVENT";

    private static final String BUNDLE_KEY_PLACE = "PLACE";
    private static final String BUNDLE_KEY_DATE_PICKED = "DATE_PICKED";
    private static final String BUNDLE_KEY_TIME_PICKED = "TIME_PICKED";
    private static final String BUNDLE_KEY_YEAR = "YEAR";
    private static final String BUNDLE_KEY_MONTH = "MONTH";
    private static final String BUNDLE_KEY_DAY = "DAY";
    private static final String BUNDLE_KEY_HOUR = "HOUR";
    private static final String BUNDLE_KEY_MINUTE = "MINUTE";
    private static final String BUNDLE_KEY_GUESTS = "GUESTS";

    private ImageButton btn_date;
    private int year_x, month_x, day_x;
    private static final int DILOG_ID_date = 0;

    private ImageButton btn_time;
    private int hour_x, minute_x;
    private static final int DILOG_ID_time = 1;

    private ImageButton buttonPickPlace;
    private int PLACE_PICKER_REQUEST = 1;

    private ImageButton buttonSave;
    private TextView dateText;
    private TextView timeText;
    private TextView placeText;

    private RestHelper restHelper;
    private RequestHandle handle;
    private EventHandler eventHandler;
    private DeleteHandler deleteHandler;
    private LatLng latLng;
    private boolean datePicked;
    private boolean timePicked;
    private LoginResponse login;

    private static final String TAG = "AddEventActivity";

    private ImageButton friends_invite;
    private DecimalFormat decimalFormat;
    private GuestsFragment guestsFragment;

    private Event intentEvent;
    private boolean isUserHost;
    private TextView eventName;
    private TextView descriptionName;
    private View buttonDelete;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        buttonSave = (ImageButton) findViewById(R.id.save_button);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        dateText = (TextView) findViewById(R.id.showDate);
        timeText = (TextView) findViewById(R.id.showTime);
        placeText = (TextView) findViewById(R.id.showPlace);

        eventName = (TextView) findViewById(R.id.eventName);
        descriptionName = (TextView) findViewById(R.id.descriptionName);

        intentEvent = getIntent().getParcelableExtra(INTENT_KEY_EVENT);

        final Calendar cal = Calendar.getInstance();
        if(intentEvent != null) {
            if(intentEvent.getDate() != null) {
                cal.setTime(intentEvent.getDate());
                datePicked = true;
                timePicked = true;
            }
            double lat = 0.0;
            double lng = 0.0;
            boolean latParsed = false;
            boolean lngParsed = false;
            try {
                lat = Location.convert(intentEvent.getLatitude());
                latParsed = true;
            } catch (Exception e) {
                Log.e(TAG, String.format("Converting latitude of Event %s failed", intentEvent.getId()), e);
            }
            try {
                lng = Location.convert(intentEvent.getLongitude());
                lngParsed = true;
            } catch (Exception e) {
                Log.e(TAG, String.format("Converting latitude of Event %s failed", intentEvent.getId()), e);
            }
            if(latParsed && lngParsed) {
                latLng = new LatLng(lat, lng);
            }
        }

        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH);
        day_x = cal.get(Calendar.DAY_OF_MONTH);

        hour_x = cal.get(Calendar.HOUR_OF_DAY);
        minute_x = cal.get(Calendar.MINUTE);

        if (savedInstanceState != null) {
            latLng = savedInstanceState.getParcelable(BUNDLE_KEY_PLACE);
            datePicked = savedInstanceState.getBoolean(BUNDLE_KEY_DATE_PICKED);
            timePicked = savedInstanceState.getBoolean(BUNDLE_KEY_TIME_PICKED);
            year_x = savedInstanceState.getInt(BUNDLE_KEY_YEAR);
            month_x = savedInstanceState.getInt(BUNDLE_KEY_MONTH);
            day_x = savedInstanceState.getInt(BUNDLE_KEY_DAY);
            hour_x = savedInstanceState.getInt(BUNDLE_KEY_HOUR);
            minute_x = savedInstanceState.getInt(BUNDLE_KEY_MINUTE);

            guestsFragment = (GuestsFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_guests);
            if(guestsFragment == null) {
                guestsFragment = new GuestsFragment();
            }
            guestsFragment.setGuests(savedInstanceState.getStringArrayList(BUNDLE_KEY_GUESTS));

        } else {
            guestsFragment = new GuestsFragment();
        }

        showDialogOnButtonClickDate();
        showTimePickerDiaglog();
        addFriends();

        eventHandler = new EventHandler(this);
        deleteHandler = new DeleteHandler(this);
        login = Globals.ENCRYPTED_STORAGE.get(getString(R.string.storage_key_login_data));
        restHelper = new RestHelper(this, login);

        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setDecimalSeparator('.');
        decimalFormat = new DecimalFormat("#.######", symbols);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("".equals(eventName.getText().toString()) || !datePicked || !timePicked || latLng == null) {
                    Toast.makeText(AddEventActivity.this, getString(R.string.missing_fields), Toast.LENGTH_SHORT).show();
                    return;
                }

                Date now = Calendar.getInstance().getTime();
                Date pickedDate = getPickedDateTime();
                if (pickedDate.before(now)) {
                    Toast.makeText(AddEventActivity.this, getString(R.string.date_in_past), Toast.LENGTH_SHORT).show();
                    return;
                }

                Event event = new Event();
                event.setName(eventName.getText().toString());
                event.setDescription(descriptionName.getText().toString());
                event.setDate(pickedDate);
                event.setInvitedUsers(new HashSet<>(guestsFragment.getGuests()));
                event.setLatitude(decimalFormat.format(latLng.latitude));
                event.setLongitude(decimalFormat.format(latLng.longitude));

                if (handle == null || handle.isCancelled() || handle.isFinished()) {
                    if(intentEvent == null) {
                        handle = restHelper.postObjectAuthorized(Constants.Urls.EVENTS, event, eventHandler);
                    } else if(isUserHost) {
                        handle = restHelper.putObjectAuthorized(Constants.Urls.EVENTS, intentEvent.getId(), event, eventHandler);
                    }
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });

        buttonDelete = findViewById(R.id.delete_button);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(handle == null || handle.isCancelled() || handle.isFinished()) {
                    if (isUserHost) {
                        handle = restHelper.deleteObjectAuthorized(Constants.Urls.EVENTS, intentEvent.getId(), deleteHandler);
                    } else {
                        handle = restHelper.deleteObjectAuthorized(Constants.Urls.INVITATIONS, intentEvent.getInvitationId(), deleteHandler);
                    }
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });

        buttonPickPlace = (ImageButton) findViewById(R.id.button_place);
        buttonPickPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                //Intent intent;
                try {
                    //intent = builder.build((Activity) getApplicationContext());
                    startActivityForResult(builder.build(AddEventActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    Log.e(TAG, "play services error", e);
                }
            }
        });

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                Log.i(TAG, "back stack changed ");
                int backCount = getSupportFragmentManager().getBackStackEntryCount();
                if (backCount == 0){
                    updateFriendText();
                }
            }
        });

        if(intentEvent != null) {
            isUserHost = login.getUsername().equals(intentEvent.getHostUserName());

            if(savedInstanceState == null) {
                eventName.setText(intentEvent.getName());
                descriptionName.setText(intentEvent.getDescription());
                //intentEvent.getInvitedUsers().remove(login.getUsername());
                guestsFragment.setGuests(intentEvent.getInvitedUsers());
            }

            TextView hostView = (TextView) findViewById(R.id.text_host);
            hostView.setVisibility(View.VISIBLE);
            hostView.setText(String.format(getString(R.string.host), intentEvent.getHostUserName()));

            if(!isUserHost) {
                setReadonlyMode();
            } else {
                guestsFragment.setEditable(true);
            }
        } else {
            guestsFragment.setEditable(true);
            buttonDelete.setVisibility(View.GONE);
        }

        updatePlaceText();
        updateDateText();
        updateTimeText();
        updateFriendText();

        setTitle("");
    }

    private void setReadonlyMode() {
        buttonSave.setVisibility(View.GONE);
        buttonPickPlace.setVisibility(View.INVISIBLE);
        btn_time.setVisibility(View.INVISIBLE);
        btn_date.setVisibility(View.INVISIBLE);
        eventName.setKeyListener(null);
        if(intentEvent.getDescription() == null || intentEvent.getDescription().equals("")) {
            descriptionName.setVisibility(View.GONE);
        } else {
            descriptionName.setKeyListener(null);
        }
        if(guestsFragment.getGuests().size() == 0) {
            friends_invite.setVisibility(View.INVISIBLE);
        }
        guestsFragment.setEditable(false);

        int bgColor = ContextCompat.getColor(this, R.color.colorContentBackground);
        findViewById(R.id.wrapper_pick_time).setBackgroundColor(bgColor);
        findViewById(R.id.wrapper_pick_place).setBackgroundColor(bgColor);
        findViewById(R.id.wrapper_pick_date).setBackgroundColor(bgColor);
    }

    private void updateFriendText() {
        String text;
        if(intentEvent != null || guestsFragment.getGuests().size() > 0) {
            if(guestsFragment.getGuests().size() == 1) {
                text = getString(R.string.one_friend);
            } else {
                text = String.format(getString(R.string.count_friends), guestsFragment.getGuests().size());
            }
        } else {
            text = getString(R.string.pick_friends);
        }
        ((TextView)findViewById(R.id.show_friends)).setText(text);
    }

    private void addFriends() {
        friends_invite = (ImageButton) findViewById(R.id.add_friend);
        friends_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_bottom, R.anim.exit_to_bottom,
                                R.anim.enter_from_bottom, R.anim.exit_to_bottom)
                        .replace(R.id.fragment_guests, guestsFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    private Date getPickedDateTime() {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(year_x, month_x, day_x, hour_x, minute_x, 0);
        return cal.getTime();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                this.latLng = place.getLatLng();
                updatePlaceText();
            }
        }
    }

    private void updatePlaceText() {
        if (latLng != null) {
            placeText.setText(String.format(getString(R.string.latlng), latLng.latitude, latLng.longitude));
        }
    }

    // DatePicker Window
    public void showDialogOnButtonClickDate() {
        btn_date = (ImageButton) findViewById(R.id.button_date);

        btn_date.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog(DILOG_ID_date);
                    }
                }
        );
    }


    // TimePicker Window
    public void showTimePickerDiaglog() {
        btn_time = (ImageButton) findViewById(R.id.button_time);

        btn_time.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog(DILOG_ID_time);
                    }
                }
        );
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == DILOG_ID_time) {
            return new TimePickerDialog(AddEventActivity.this, R.style.AlertDialogTheme ,kTimePickerListener, hour_x, minute_x, true);
        } else if (id == DILOG_ID_date) {
            return new DatePickerDialog(AddEventActivity.this, R.style.AlertDialogTheme, dpickerListener, year_x, month_x, day_x);
        } else {
            return null;
        }
    }

    // Datepicking
    protected DatePickerDialog.OnDateSetListener dpickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            year_x = year;
            month_x = monthOfYear;
            day_x = dayOfMonth;
            datePicked = true;

            updateDateText();
        }
    };

    private void updateDateText() {
        if (datePicked) {
            String day_x_string = String.valueOf(day_x);
            String month_x_string = String.valueOf(month_x + 1);
            String year_x_string = String.valueOf(year_x);

            String date = day_x_string + "." + month_x_string + "." + year_x_string;

            dateText.setText(date);
        }
    }

    // Timepicking
    protected TimePickerDialog.OnTimeSetListener kTimePickerListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            hour_x = hourOfDay;
            minute_x = minute;
            timePicked = true;

            updateTimeText();
        }
    };

    private void updateTimeText() {
        if (timePicked) {
            String hour_x_string = String.valueOf(hour_x);
            String minute_x_string = String.valueOf(minute_x);
            String time = hour_x_string + ":" + minute_x_string;

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.GERMANY);
            try {
                Date date = sdf.parse(time);
                String formattedTime = sdf.format(date);
                timeText.setText(formattedTime);
            } catch (ParseException e) {
                timeText.setText(time);
            }
        }
    }

    private void startGeoService() {
        Intent geoIntent = new Intent(this, GeoService.class);
        geoIntent.putExtra("type", "getEvents");
        startService(geoIntent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handle != null) {
            handle.cancel(true);
        }
    }

    private static class EventHandler extends GsonHttpResponseHandler<Event> {
        private WeakReference<AddEventActivity> activity;

        EventHandler(AddEventActivity activity) {
            super(Event.class, Globals.GSON);
            this.activity = new WeakReference<>(activity);
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "Event could not be created/updated", throwable);
            AddEventActivity activity = this.activity.get();
            if (activity != null) {
                Toast.makeText(activity, String.format(activity.getString(R.string.error_status_code), statusCode), Toast.LENGTH_SHORT).show();
                activity.progressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, Event event) {
            Log.i(TAG, "Event created/updated");
            finishActivity(event);
        }

        @Override
        public void onSuccessParsingFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "response could not be parsed", throwable);
            finishActivity(null);
        }

        private void finishActivity(@Nullable Event event) {
            AddEventActivity activity = this.activity.get();
            if (activity != null) {

                String name = "";
                if (event != null) {
                    name = event.getName();
                }
                Toast.makeText(activity, String.format(activity.getString(R.string.event_saved), name), Toast.LENGTH_SHORT).show();

                // refresh markers on the map
                activity.startGeoService();
                
                activity.finish();
            }
        }
    }

    private static class DeleteHandler extends TextHttpResponseHandler {
        private WeakReference<AddEventActivity> activity;

        DeleteHandler(AddEventActivity activity) {
            this.activity = new WeakReference<>(activity);
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "Event/Invitation could not be deleted", throwable);
            AddEventActivity activity = this.activity.get();
            if (activity != null) {
                Toast.makeText(activity, String.format(activity.getString(R.string.error_status_code), statusCode), Toast.LENGTH_SHORT).show();
                activity.progressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, String responseString) {
            Log.i(TAG, "Event/Invitation deleted");
            AddEventActivity activity = this.activity.get();
            if (activity != null) {
                Toast.makeText(activity, activity.getString(R.string.deleted), Toast.LENGTH_SHORT).show();

                // refresh markers on the map
                activity.startGeoService();

                activity.finish();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_KEY_PLACE, latLng);
        outState.putBoolean(BUNDLE_KEY_DATE_PICKED, datePicked);
        outState.putBoolean(BUNDLE_KEY_TIME_PICKED, timePicked);
        outState.putInt(BUNDLE_KEY_YEAR, year_x);
        outState.putInt(BUNDLE_KEY_MONTH, month_x);
        outState.putInt(BUNDLE_KEY_DAY, day_x);
        outState.putInt(BUNDLE_KEY_HOUR, hour_x);
        outState.putInt(BUNDLE_KEY_MINUTE, minute_x);
        outState.putStringArrayList(BUNDLE_KEY_GUESTS, guestsFragment.getGuests());
    }
}
