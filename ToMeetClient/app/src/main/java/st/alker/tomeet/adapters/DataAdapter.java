package st.alker.tomeet.adapters;

import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;

public abstract class DataAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    protected ArrayList<T> items = new ArrayList<>();

    public DataAdapter() {

    }

    public DataAdapter(ArrayList<T> items) {
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(T t) {
        items.add(t);
        notifyItemInserted(items.size() - 1);
    }

    public void addAll(Collection<T> items) {
        int position = this.items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(position, items.size());
    }

    public void clear() {
        int itemCount = items.size();
        items.clear();
        notifyItemRangeRemoved(0, itemCount);
    }

    public void setItems(Collection<T> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public T remove(int i) {
        T t = items.remove(i);
        notifyItemRemoved(i);
        return t;
    }

    public boolean remove(T t) {
        int index = items.indexOf(t);
        if(index >= 0) {
            items.remove(index);
            notifyItemRemoved(index);
            return true;
        } else {
            return false;
        }
    }
}
