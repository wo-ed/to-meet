package st.alker.tomeet.rest.entities;

import com.google.gson.annotations.SerializedName;

// If needed create separate classes for get/post/put/delete (different parameters)
public class TrackingPermission {
    @SerializedName("Id")
    private int id;
    @SerializedName("UserName")
    private String userName;
    @SerializedName("DeviceId")
    private int deviceId;
    @SerializedName("Device")
    private Device device;
    @SerializedName("GPSPermissionGranted")
    private boolean gpsPermissionGranted;
    @SerializedName("MACPermissionGranted")
    private boolean macPermissionGranted;
    @SerializedName("MACBluetooth")
    private String macBluetooth;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public Device getDevice() {
        return device;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public boolean isGpsPermissionGranted() {
        return gpsPermissionGranted;
    }

    public void setGpsPermissionGranted(boolean gpsPermissionGranted) {
        this.gpsPermissionGranted = gpsPermissionGranted;
    }

    public boolean isMacPermissionGranted() {
        return macPermissionGranted;
    }

    public void setMacPermissionGranted(boolean macPermissionGranted) {
        this.macPermissionGranted = macPermissionGranted;
    }

    public String getMacBluetooth() {
        return macBluetooth;
    }

    public void setMacBluetooth(String macBluetooth) {
        this.macBluetooth = macBluetooth;
    }
}
