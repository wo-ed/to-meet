package st.alker.tomeet.utils;

import android.support.annotation.Nullable;
import com.google.gson.*;
import st.alker.tomeet.Globals;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

public class GsonDateAdapter implements JsonSerializer<Date>,JsonDeserializer<Date> {
    private final DateFormat dateFormat;

    public GsonDateAdapter(DateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    public synchronized JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(dateFormat.format(date));
    }

    @Override
    @Nullable
    public synchronized Date deserialize(JsonElement jsonElement,Type type,JsonDeserializationContext jsonDeserializationContext) {
        try {
            return dateFormat.parse(jsonElement.getAsString());
        } catch (ParseException e) {
            return null;
        }
    }
}