package st.alker.tomeet.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.*;
import android.util.Log;
import android.view.*;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.github.fabtransitionactivity.SheetLayout;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.TextHttpResponseHandler;
import cz.msebera.android.httpclient.Header;
import st.alker.tomeet.Globals;
import st.alker.tomeet.R;
import st.alker.tomeet.activities.AddFriendActivity;
import st.alker.tomeet.adapters.FriendsAdapter;
import st.alker.tomeet.alarm.AlarmService;
import st.alker.tomeet.rest.Constants;
import st.alker.tomeet.rest.GsonHttpResponseHandler;
import st.alker.tomeet.rest.RestHelper;
import st.alker.tomeet.rest.entities.LoginResponse;
import st.alker.tomeet.rest.entities.TrackingPermission;
import st.alker.tomeet.utils.BluetoothUtil;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class FriendsFragment extends Fragment implements FriendsAdapter.OnClickListener {
    private static final int REQUEST_ADD_FRIEND = 0;

    private RestHelper restHelper;
    private PermissionsGetHandler permissionsHandler;
    private DeleteHandler deleteHandler;
    private FriendsAdapter adapter;
    private RequestHandle getHandle;
    private RequestHandle deleteHandle;
    private LoginResponse login;
    private ProgressBar progressBar;
    private FriendsAdapter.InteractionListener listener;
    private String macBluetooth;
    private SwipeRefreshLayout swipeContainer;
    private FloatingActionButton fab;
    private SheetLayout sheetLayout;
    private RecyclerView recyclerView;
    private int deletePermissionid = -1;
    private int deleteItemPosition = -1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new FriendsAdapter(getContext(), listener, this);

        macBluetooth = BluetoothUtil.getBluetoothMacAddress(getContext().getContentResolver());

        login = Globals.ENCRYPTED_STORAGE.get(getString(R.string.storage_key_login_data));
        restHelper = new RestHelper(getContext(), login);
        permissionsHandler = new PermissionsGetHandler(this);
        deleteHandler = new DeleteHandler(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_friends, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        recyclerView = (RecyclerView) view.findViewById(R.id.list_friends);
        recyclerView.setAdapter(adapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider));
        recyclerView.addItemDecoration(dividerItemDecoration);

        fab = (FloatingActionButton) view.findViewById(R.id.fab);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    // Scroll Down
                    if (fab.isShown()) {
                        fab.hide();
                    }
                } else if (dy < 0) {
                    // Scroll Up
                    if (!fab.isShown()) {
                        fab.show();
                    }
                }
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sheetLayout.setVisibility(View.VISIBLE);
                sheetLayout.expandFab();
            }
        });

        sheetLayout = (SheetLayout) view.findViewById(R.id.bottom_sheet);
        sheetLayout.setFab(fab);
        sheetLayout.setFabAnimationEndListener(new SheetLayout.OnFabAnimationEndListener() {
            @Override
            public void onFabAnimationEnd() {
                Intent intent = new Intent(getContext(), AddFriendActivity.class);
                startActivityForResult(intent, REQUEST_ADD_FRIEND);
            }
        });

        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeContainer.setColorSchemeColors(
                ContextCompat.getColor(getContext(), R.color.colorAccent),
                ContextCompat.getColor(getContext(), R.color.colorPrimary));

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                refresh();
            }
        });
    }

    private void refresh() {
        progressBar.setVisibility(View.GONE);
        getHandle.cancel(true);
        getHandle = restHelper.getObjectsAuthorized(Constants.Urls.TRACKING_PERMISSIONS, permissionsHandler);
    }

    private void resetTrackingPermissions(TrackingPermission[] permissions) {
        ArrayList<FriendsAdapter.Friend> list = new ArrayList<>();

        for (TrackingPermission permission : permissions) {
            if (!isUserDeviceOwner(permission)) {
                int id = -1;
                for (TrackingPermission otherPermission : permissions) {
                    if (isUserDeviceOwner(otherPermission)
                            && otherPermission.getDevice().getMacBluetooth().equals(macBluetooth)
                            && otherPermission.getUserName().equals(permission.getDevice().getUserName())) {
                        id = otherPermission.getId();
                        break;
                    }
                }
                if (id != -1) {
                    list.add(createFriendFromFriendPermission(permission, id, true));
                } else {
                    boolean friendRequestExists = false;
                    for (FriendsAdapter.Friend friend : list) {
                        if (friend.getUserName().equals(permission.getDevice().getUserName())) {
                            friendRequestExists = true;
                            break;
                        }
                    }
                    if (!friendRequestExists) {
                        list.add(createFriendFromFriendPermission(permission, id, false));
                    }
                }
            } else {
                if (!Objects.equals(macBluetooth, permission.getDevice().getMacBluetooth())) {
                    continue;
                }
                boolean permissionGranted = false;
                for (TrackingPermission other : permissions) {
                    if (permission.getUserName().equals(other.getDevice().getUserName())) {
                        permissionGranted = true;
                        break;
                    }
                }
                if (!permissionGranted) {
                    list.add(createFriendFromOwnPermission(permission));
                }
            }
        }

        Collections.sort(list);
        adapter.setItems(list);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        sheetLayout.setVisibility(View.GONE);
        fab.setVisibility(View.VISIBLE);
    }

    private boolean isUserDeviceOwner(TrackingPermission permission) {
        return login.getUsername().equals(permission.getDevice().getUserName());
    }

    private FriendsAdapter.Friend createFriendFromFriendPermission(TrackingPermission permission, int permissionId, boolean added) {
        FriendsAdapter.Friend friend = new FriendsAdapter.Friend();
        friend.setPermissionId(permissionId);
        friend.setUserName(permission.getDevice().getUserName());
        friend.setMacPermissionGranted(permission.isMacPermissionGranted());
        friend.setGpsPermissionGranted(permission.isGpsPermissionGranted());
        friend.setDevice(permission.getDevice());
        friend.setAdded(added);
        return friend;
    }

    private FriendsAdapter.Friend createFriendFromOwnPermission(TrackingPermission permission) {
        FriendsAdapter.Friend friend = new FriendsAdapter.Friend();
        friend.setPermissionId(permission.getId());
        friend.setUserName(permission.getUserName());
        friend.setAdded(true);
        return friend;
    }

    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.VISIBLE);
        getHandle = restHelper.getObjectsAuthorized(Constants.Urls.TRACKING_PERMISSIONS, permissionsHandler);
    }

    @Override
    public void onPause() {
        super.onPause();
        getHandle.cancel(true);

        if(deleteHandle != null) {
            deleteHandle.cancel(true);
        }
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (FriendsAdapter.InteractionListener) context;
    }

    @Override
    public boolean onLongClick(View view, int permissionId, int position) {
        this.deletePermissionid = permissionId;
        this.deleteItemPosition = position;
        showPopup(view);
        return true;
    }

    private void showPopup(View v) {
        PopupMenu popup = new PopupMenu(getContext(), v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_friends, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_item_delete:
                        if(deleteHandle != null) {
                            deleteHandle.cancel(true);
                        }
                        restHelper.deleteObjectAuthorized(Constants.Urls.TRACKING_PERMISSIONS, deletePermissionid, deleteHandler);
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    private static class PermissionsGetHandler extends GsonHttpResponseHandler<TrackingPermission[]> {
        private static final String TAG = "PermissionsGetHandler";

        private WeakReference<FriendsFragment> fragment;

        PermissionsGetHandler(FriendsFragment fragment) {
            super(TrackingPermission[].class, Globals.GSON);
            this.fragment = new WeakReference<>(fragment);
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, TrackingPermission[] permissions) {
            Log.i(TAG, "TrackingPermissions were retrieved successfully");
            FriendsFragment fragment = this.fragment.get();
            if (fragment != null) {
                fragment.resetTrackingPermissions(permissions);
                fragment.swipeContainer.setRefreshing(false);
            }
        }

        @Override
        public void onSuccessParsingFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "response could not be parsed", throwable);
            handleFailure(statusCode);
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "TrackingPermissions could not be retrieved", throwable);
            handleFailure(statusCode);
        }

        private void handleFailure(int statusCode) {
            FriendsFragment fragment = this.fragment.get();
            if (fragment != null) {
                Context ctx = fragment.getContext();
                Toast.makeText(ctx, String.format(ctx.getString(R.string.error_status_code), statusCode), Toast.LENGTH_SHORT).show();
                fragment.progressBar.setVisibility(View.GONE);
                fragment.swipeContainer.setRefreshing(false);
            }
        }
    }

    private static class DeleteHandler extends TextHttpResponseHandler {
        private static final String TAG = "DeleteHandler";

        private WeakReference<FriendsFragment> fragment;

        DeleteHandler(FriendsFragment fragment) {
            this.fragment = new WeakReference<>(fragment);
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "Permission could not be deleted", throwable);
            FriendsFragment fragment = this.fragment.get();
            if (fragment != null) {
                Context ctx = fragment.getContext();
                Toast.makeText(ctx, String.format(ctx.getString(R.string.error_status_code), statusCode), Toast.LENGTH_SHORT).show();
                //activity.progressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, String responseString) {
            Log.i(TAG, "Permission deleted");
            FriendsFragment fragment = this.fragment.get();
            if (fragment != null) {
                fragment.adapter.remove(fragment.deleteItemPosition);
                fragment.refresh();

                // Update map markers
                Intent alarmIntent = new Intent(fragment.getContext(), AlarmService.class);
                alarmIntent.putExtra("type", "getFriends");
                fragment.getContext().startService(alarmIntent);
            }
        }
    }
}
