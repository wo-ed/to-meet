package st.alker.tomeet.geofence;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class GeoReceiver extends BroadcastReceiver {
    protected static final String TAG = "GeoReceiver";

    /**
     * Receives the PendingIntent of the Geofencing event
     * Passes is on to the GeofenceTransitionsIntentService
     * @param context context
     * @param intent contains the Geofencing event
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Receiver received broadcast");
        Intent i = new Intent(context, GeofenceTransitionsIntentService.class);
        i.putExtra("geoIntent", intent);
        context.startService(i);
    }
}
