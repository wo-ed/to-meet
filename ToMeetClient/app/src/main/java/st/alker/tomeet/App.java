package st.alker.tomeet;

import android.app.Application;
import com.orhanobut.hawk.Hawk;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Globals.ENCRYPTED_STORAGE.initialize(this);
    }
}

