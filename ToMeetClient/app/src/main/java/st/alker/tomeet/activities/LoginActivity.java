package st.alker.tomeet.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;
import st.alker.tomeet.Globals;
import st.alker.tomeet.R;
import st.alker.tomeet.rest.GsonHttpResponseHandler;
import st.alker.tomeet.rest.RestHelper;
import st.alker.tomeet.rest.entities.LoginRequest;
import st.alker.tomeet.rest.entities.LoginResponse;
import st.alker.tomeet.utils.NetworkUtil;

import java.lang.ref.WeakReference;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    public static final int MINIMUM_PASSWORD_LENGTH = 6;

    private EditText userNameView;
    private EditText passwordView;
    private View progressView;
    private View loginFormView;

    private RestHelper restHelper;
    private boolean busy = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userNameView = (EditText) findViewById(R.id.username);

        passwordView = (EditText) findViewById(R.id.password);
        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mSignInButton = (Button) findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        loginFormView = findViewById(R.id.login_form);
        progressView = findViewById(R.id.login_progress);

        restHelper = new RestHelper(this);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setElevation(0);
        }
    }

    private void setBusy(boolean busy) {
        this.busy = busy;
        showProgress(this.busy);
    }

    private boolean isBusy() {
        return this.busy;
    }

    private void attemptLogin() {
        if(busy) {
            return;
        }

        if(!NetworkUtil.isNetworkAvailable(this)) {
            Toast.makeText(this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
            return;
        }

        userNameView.setError(null);
        passwordView.setError(null);

        String userName = userNameView.getText().toString();
        String password = passwordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(password)) {
            passwordView.setError(getString(R.string.error_field_required));
            focusView = passwordView;
            cancel = true;
        } /*else if (!isPasswordValid(password)) {
            passwordView.setError(getString(R.string.error_invalid_password));
            focusView = passwordView;
            cancel = true;
        }*/

        if (TextUtils.isEmpty(userName)) {
            userNameView.setError(getString(R.string.error_field_required));
            focusView = userNameView;
            cancel = true;
        } /*else if (!isUserNameValid(userName)) {
            userNameView.setError(getString(R.string.error_invalid_username));
            focusView = userNameView;
            cancel = true;
        }*/

        if (cancel) {
            focusView.requestFocus();
        } else {
            setBusy(true);
            LoginRequest login = new LoginRequest(
                    userNameView.getText().toString(),
                    passwordView.getText().toString());
            restHelper.requestAccessToken(login, new LoginResponseHandler(this));
        }
    }

    private boolean isUserNameValid(String userName) {
        //TODO: Replace this with your own logic
        return true;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= MINIMUM_PASSWORD_LENGTH;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            loginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private static class LoginResponseHandler extends GsonHttpResponseHandler<LoginResponse> {
        private WeakReference<LoginActivity> activity;

        private LoginResponseHandler(LoginActivity activity) {
            super(LoginResponse.class, Globals.GSON);
            this.activity = new WeakReference<>(activity);
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            String userMessage = null;
            String logMessage = null;
            try {
                JSONObject obj = new JSONObject(responseString);
                userMessage = obj.getString("error_description");
                logMessage = userMessage;
            } catch (JSONException | NullPointerException ex) {
                if(activity.get() != null) {
                    userMessage = String.format(activity.get().getString(R.string.error_status_code), statusCode);
                }
                logMessage = userMessage + responseString;
            }
            handleFailure(userMessage, logMessage, throwable);
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, LoginResponse loginResponse) {
            Log.i(TAG, "Login was successful");
            LoginActivity activity = this.activity.get();
            if(activity != null) {
                Globals.ENCRYPTED_STORAGE.put(activity.getString(R.string.storage_key_login_data), loginResponse);
                activity.startActivity(new Intent(activity, MainActivity.class));
                activity.finish();
            }
        }

        @Override
        public void onSuccessParsingFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            String userMessage = null;
            if(activity.get() != null) {
                userMessage = String.format(activity.get().getString(R.string.internal_error_status_code), 1);
            }
            String logMessage = "Login success response parsing failure; response string: " + responseString;
            handleFailure(userMessage, logMessage, throwable);
        }

        private void handleFailure(String userMessage, String logMessage, Throwable throwable) {
            LoginActivity activity = this.activity.get();
            if(activity != null) {
                activity.setBusy(false);
                Log.e(TAG, logMessage, throwable);
                Toast.makeText(activity, userMessage, Toast.LENGTH_SHORT).show();
            }
        }
    }
}

