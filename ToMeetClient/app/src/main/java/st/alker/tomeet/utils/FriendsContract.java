package st.alker.tomeet.utils;

import android.provider.BaseColumns;

/**
 * Created by sael on 15.12.2016.
 */

public class FriendsContract {

    private FriendsContract(){};

    public static class FriendsEntry implements BaseColumns {
        public static final String TABLE_NAME = "friends";
        public static final String NAME_COLUMN = "name";
        public static final String MAC_COLUMN = "mac";
    }
}
