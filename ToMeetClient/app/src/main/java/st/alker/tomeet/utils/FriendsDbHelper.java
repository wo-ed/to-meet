package st.alker.tomeet.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sael on 15.12.2016.
 */

public class FriendsDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Friends.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + FriendsContract.FriendsEntry.TABLE_NAME + " (" +
                    FriendsContract.FriendsEntry._ID + " INTEGER PRIMARY KEY," +
                    FriendsContract.FriendsEntry.NAME_COLUMN + TEXT_TYPE + COMMA_SEP  +
                    FriendsContract.FriendsEntry.MAC_COLUMN + TEXT_TYPE + " )";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + FriendsContract.FriendsEntry.TABLE_NAME;

    private SQLiteDatabase mSQLiteDatabase;


    private ArrayAdapter<String> allFriends;
    List<String> deviceName;

    public FriendsDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mSQLiteDatabase = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase){
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int j){
        sqLiteDatabase.execSQL(SQL_DELETE_ENTRIES);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onDowngrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion){
        onUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }

    public String getFriendsMacAddress(String nickname){
        String[] projection = { FriendsContract.FriendsEntry.MAC_COLUMN};

        String selection = FriendsContract.FriendsEntry.NAME_COLUMN + " = ?";
        String[] selectionArgs = { nickname };

        Cursor c = mSQLiteDatabase.query(
                FriendsContract.FriendsEntry.TABLE_NAME,        // Datenbank Name
                projection,                                     // Welche Spalte soll ausgegeben werden
                selection,
                selectionArgs,
                null,
                null,
                null
                );

        c.moveToFirst();
        if (c.getCount() == 0){
            return null;
        }
        return c.getString(0);
    }

    public String getFriendsUserName(String nickname){
        String[] projection = { FriendsContract.FriendsEntry.NAME_COLUMN};

        String selection = FriendsContract.FriendsEntry.MAC_COLUMN + " = ?";
        String[] selectionArgs = { nickname };

        Cursor c = mSQLiteDatabase.query(
                FriendsContract.FriendsEntry.TABLE_NAME,        // Datenbank Name
                projection,                                     // Welche Spalte soll ausgegeben werden
                selection,                                      // In welcher Spalte soll gesucht werden
                selectionArgs,                                  // Nach was soll gesucht werden
                null,
                null,
                null
        );

        c.moveToFirst();
        if (c.getCount() == 0){
            return null;
        }
        return c.getString(0);
    }

    public List<String> getAllFriendsUserName(long id){

        SQLiteDatabase mSQLiteDatabase = this.getReadableDatabase();
        Cursor c = mSQLiteDatabase.rawQuery("select * from " + FriendsContract.FriendsEntry.TABLE_NAME, null);

        deviceName = new ArrayList<String>();
        if (c.moveToFirst()){
            deviceName.add(c.getString(c.getColumnIndex(FriendsContract.FriendsEntry.NAME_COLUMN)));
            while(c.moveToNext()){
                deviceName.add(c.getString(c.getColumnIndex(FriendsContract.FriendsEntry.NAME_COLUMN)));
            }
        }
        return deviceName;
    }

    public List<String> getAllFriendsMacAddress(long id){

        SQLiteDatabase mSQLiteDatabase = this.getReadableDatabase();
        Cursor c = mSQLiteDatabase.rawQuery("select * from " + FriendsContract.FriendsEntry.TABLE_NAME, null);

        deviceName = new ArrayList<String>();
        if (c.moveToFirst()){
            deviceName.add(c.getString(c.getColumnIndex(FriendsContract.FriendsEntry.MAC_COLUMN)));
            while(c.moveToNext()){
                deviceName.add(c.getString(c.getColumnIndex(FriendsContract.FriendsEntry.MAC_COLUMN)));
            }
        }
        return deviceName;
    }

    public long addFriend(String nickname, String adress){
        ContentValues mContentValues = new ContentValues();
        mContentValues.put(FriendsContract.FriendsEntry.NAME_COLUMN, nickname);
        mContentValues.put(FriendsContract.FriendsEntry.MAC_COLUMN, adress);
        return mSQLiteDatabase.insert(FriendsContract.FriendsEntry.TABLE_NAME, null, mContentValues);
    }

    public boolean updateFriends(long id, String text){
        ContentValues mContentValues = new ContentValues();
        mContentValues.put(FriendsContract.FriendsEntry.NAME_COLUMN, text);

        String selection = FriendsContract.FriendsEntry._ID + " =?";
        String[] selectionArgs = {Long.toString(id)};

        int count = mSQLiteDatabase.update(
                FriendsContract.FriendsEntry.TABLE_NAME,
                mContentValues,
                selection,
                selectionArgs);
        return count > 0;
    }

    public boolean deleteFriend(String userName){
        String selection = FriendsContract.FriendsEntry.NAME_COLUMN + " = ?";
        String[] selectionArgs = { userName };
        int count = mSQLiteDatabase.delete(FriendsContract.FriendsEntry.TABLE_NAME, selection, selectionArgs);
        return count > 0;
    }

}
