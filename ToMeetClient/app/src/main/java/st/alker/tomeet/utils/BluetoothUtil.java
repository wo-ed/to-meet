package st.alker.tomeet.utils;

import android.content.ContentResolver;
import android.util.Log;

public class BluetoothUtil {
    private static final String TAG = "BluetoothUtil";
    public static final String DEFAULT_MAC = "02:00:00:00:00:00";

    public static String getBluetoothMacAddress(ContentResolver contentResolver) {
        String mac = null;
        try {
            mac = android.provider.Settings.Secure.getString(contentResolver, "bluetooth_address");
        } catch (SecurityException ex) {
            Log.e(TAG, "BluetoothAddress could not be retrieved", ex);
        }
        if(mac == null || mac.equals("")) {
            mac = DEFAULT_MAC;
        }
        return mac;
    }
}
