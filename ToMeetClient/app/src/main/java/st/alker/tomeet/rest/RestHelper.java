package st.alker.tomeet.rest;

import android.content.Context;
import com.loopj.android.http.*;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.message.BasicHeader;
import st.alker.tomeet.Globals;
import st.alker.tomeet.rest.entities.LoginRequest;
import st.alker.tomeet.rest.entities.LoginResponse;
import st.alker.tomeet.rest.entities.RegistrationRequest;

public class RestHelper {
    private static final int TIMEOUT = 20 * 1000;

    private GsonHttpClient client;
    private Context context;
    private LoginResponse login;

    private Header[] headerArray = {
            new BasicHeader(Constants.ACCEPT, Constants.MimeTypes.JSON)
    };

    private Header[] authorizedHeaderArray;

    public RestHelper(Context context, LoginResponse login) {
        this(context);
        setLogin(login);
    }

    public RestHelper(Context context) {
        this.context = context;
        this.client = new GsonHttpClient(Globals.GSON);
        this.client.setTimeout(TIMEOUT);
    }

    public <T> RequestHandle getObjects(String url, GsonHttpResponseHandler<T[]> handler) {
        return client.getObjects(context, url, headerArray, new RequestParams(), handler);
    }

    public <T> RequestHandle getObjectsAuthorized(String url, GsonHttpResponseHandler<T[]> handler) {
        return client.getObjects(context, url, authorizedHeaderArray, new RequestParams(), handler);
    }

    public <T> RequestHandle getObject(String url, long id, GsonHttpResponseHandler<T> handler) {
        return client.getObject(context, url, id, headerArray, new RequestParams(), handler);
    }

    public <T> RequestHandle getObjectAuthorized(String url, long id, GsonHttpResponseHandler<T> handler) {
        return client.getObject(context, url, id, authorizedHeaderArray, new RequestParams(), handler);
    }

    public RequestHandle postObject(String url, Object inputParameterObj, TextHttpResponseHandler handler) {
        return client.postObject(context, url, headerArray, inputParameterObj, handler);
    }

    public RequestHandle postObjectAuthorized(String url, Object inputParameterObj, TextHttpResponseHandler handler) {
        return client.postObject(context, url, authorizedHeaderArray, inputParameterObj, handler);
    }

    public RequestHandle putObject(String url, int id, Object inputParameterObj, TextHttpResponseHandler handler) {
        return client.putObject(context, url, id, headerArray, inputParameterObj, handler);
    }

    public RequestHandle putObjectAuthorized(String url, int id, Object inputParameterObj, TextHttpResponseHandler handler) {
        return client.putObject(context, url, id, authorizedHeaderArray, inputParameterObj, handler);
    }

    public RequestHandle deleteObject(String url, long id, TextHttpResponseHandler handler) {
        return client.deleteObject(context, url, id, headerArray, new RequestParams(), handler);
    }

    public RequestHandle deleteObjectAuthorized(String url, long id, TextHttpResponseHandler handler) {
        return client.deleteObject(context, url, id, authorizedHeaderArray, new RequestParams(), handler);
    }

    public RequestHandle registerUser(RegistrationRequest registration, TextHttpResponseHandler handler) {
        return postObject(Constants.Urls.REGISTER, registration, handler);
    }

    public RequestHandle postEventArrival(int eventId, TextHttpResponseHandler handler) {
        String url = Constants.Urls.ARRIVAL + "/" + eventId;
        return client.post(context, url, authorizedHeaderArray, new RequestParams(), Constants.MimeTypes.JSON, handler);
    }

    public RequestHandle requestAccessToken(LoginRequest login, TextHttpResponseHandler handler) {
        RequestParams params = new RequestParams();
        params.put(Constants.GRANT_TYPE, Constants.GRANT_TYPE_PASSWORD);
        params.put("username", login.getUsername());
        params.put("password", login.getPassword());
        return client.post(context,
                Constants.Urls.TOKEN,
                headerArray,
                params,
                Constants.MimeTypes.X_WWW_FORM_URLENCODED,
                handler);
    }

    private Header[] getAuthorizedHeaderArray() {
        return new Header[] {
                new BasicHeader(Constants.ACCEPT, Constants.MimeTypes.JSON),
                new BasicHeader(Constants.AUTHORIZATION, login.getTokenType() + " " + login.getAccessToken())
        };
    }

    public LoginResponse getLogin() {
        return login;
    }

    public void setLogin(LoginResponse login) {
        this.login = login;
        authorizedHeaderArray = getAuthorizedHeaderArray();
    }

    public GsonHttpClient getClient() {
        return client;
    }
}
