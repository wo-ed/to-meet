package st.alker.tomeet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import st.alker.tomeet.encryption.EncryptedKeyValueStorage;
import st.alker.tomeet.encryption.HawkStorage;
import st.alker.tomeet.utils.GsonDateAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Globals {
    public static final EncryptedKeyValueStorage ENCRYPTED_STORAGE = new HawkStorage();

    public static final TimeZone TIMEZONE_SERVER = TimeZone.getTimeZone("GMT");
    public static final Locale LOCALE_SERVER = Locale.US;

    public static final SimpleDateFormat SERVER_RFC1123_DATE_FORMAT = new SimpleDateFormat(
            "EEE, dd MMM yyyy HH:mm:ss z", LOCALE_SERVER);

    public static final SimpleDateFormat SERVER_TOKEN_DATE_FORMAT = SERVER_RFC1123_DATE_FORMAT;

    public static final SimpleDateFormat SERVER_DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS", LOCALE_SERVER);

    public static final Gson GSON = new GsonBuilder().registerTypeAdapter(Date.class, new GsonDateAdapter(SERVER_DEFAULT_DATE_FORMAT)).create();

    static {
        SERVER_TOKEN_DATE_FORMAT.setTimeZone(TIMEZONE_SERVER);
        SERVER_DEFAULT_DATE_FORMAT.setTimeZone(TIMEZONE_SERVER);
    }
}
