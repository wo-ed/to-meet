package st.alker.tomeet.geofence;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import st.alker.tomeet.Globals;
import st.alker.tomeet.R;
import st.alker.tomeet.rest.RestHelper;
import st.alker.tomeet.rest.entities.Event;
import st.alker.tomeet.rest.entities.LoginResponse;
import st.alker.tomeet.utils.LocationReceiver;
import st.alker.tomeet.utils.NotificationHelper;

public class GeofenceTransitionsIntentService extends IntentService {

    protected static final String TAG = "GeofenceTransitionsIS";
    private static final Object prefSynchronizer = new Object();
    private static final Object notificationWait = new Object();
    private RestHelper restHelper;
    private TextHttpResponseHandler notificationHandler = new TextHttpResponseHandler() {

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "Failure on sending notifications to all: " + responseString + " and statuscode: " + statusCode);
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, String responseString) {
            Log.i(TAG, "Success on sending notifications to all");
            synchronized (notificationWait) {
                notificationWait.notifyAll();
            }
        }
    };

    public GeofenceTransitionsIntentService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LoginResponse login = Globals.ENCRYPTED_STORAGE.get(getString(R.string.storage_key_login_data));
        restHelper = new RestHelper(this, login);
    }

    /**
     * @param intent contains the Geofencing event
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        Intent i = intent.getParcelableExtra("geoIntent");

        GeofencingEvent g = GeofencingEvent.fromIntent(i);

        List<Geofence> list = g.getTriggeringGeofences();

        Location l = g.getTriggeringLocation();

        if (l == null) {
            //happens if the user switches the location mode (Settings-Location-Mode) while the app is running
            Log.e(TAG, "NullPointer location");
            return;
        }

        String eventLocations;
        synchronized (LocationReceiver.eventSynchronizer) {
            SharedPreferences prefs = getSharedPreferences("eventLocationData", MODE_PRIVATE);
            eventLocations = prefs.getString("eventLocations", "Abort...");
        }
        if (eventLocations.equals("Abort...")) {
            Log.e(TAG, "Aborted...eventLocations was not in sharedPrefs/we don't have any events yet");
        } else {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<Event>>() {
            }.getType();
            ArrayList<Event> eventLocationList = gson.fromJson(eventLocations, type);
            synchronized (prefSynchronizer) {

                for (Geofence geofence : list) { //go through al trigger geofences
                    eventloop:
                    for (final Event event : eventLocationList) { // go through all available events
                        if (geofence.getRequestId().equals(Integer.toString(event.getId()))) { //compare geofence id to event id
                            Date date = new Date(); //currentDate
                            long now = date.getTime();
                            long eventTime = event.getDate().getTime();
                            long delta = 120 * 60 * 1000; //delta of 120 minutes in which the geofence should trigger
                            eventTime = eventTime + (60 * 60 * 1000); //eventTime + 60 minutes for delays
                            if (eventTime - now < delta && eventTime - now > 0) { //time now is somewhere between event -1h and event +1h
                                SharedPreferences sharedPrefs = getSharedPreferences("eventNotificationData", MODE_PRIVATE);
                                String eventNotificationString = sharedPrefs.getString("sentEventNotification", "Abort...");
                                ArrayList<Event> eventNotificationList;
                                if (eventNotificationString.equals("Abort...")) {
                                    eventNotificationList = new ArrayList<>();
                                } else {
                                    eventNotificationList = gson.fromJson(eventNotificationString, type);
                                }


                                if (!eventNotificationString.equals("Abort...")) {
                                    for (Event sentEvent : eventNotificationList) { //go through all events for which we have already sent a notification
                                        if (Integer.toString(sentEvent.getId()).equals(Integer.toString(event.getId()))) { //if the id matches our current event, don't send another notification
                                            Log.w(TAG, "Not sending notifications to all, because notifications for this event have already been sent. EventName: " + event.getName());
                                            continue eventloop; // continue with the next event
                                        }
                                    }
                                }


                                /**Send notifications to everyone*/

                                new Thread() {
                                    public void run() {
                                        Log.w(TAG, event.getName());
                                        restHelper.postEventArrival(event.getId(), notificationHandler);
                                    }
                                }.start();
                                synchronized (notificationWait) {
                                    try {
                                        notificationWait.wait(30000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }


                                /**send local notification*/
                                if (NotificationHelper.notificationHelper == null) {
                                    NotificationHelper.notificationHelper = new NotificationHelper();
                                }
                                NotificationHelper.notificationHelper.notifyUser("Arrived at event " + event.getName(), event.getDescription(), this);


                                eventNotificationList.add(event);

                                SharedPreferences.Editor editor = sharedPrefs.edit();
                                String input = gson.toJson(eventNotificationList);
                                editor.remove("sentEventNotification");
                                editor.commit();
                                editor.putString("sentEventNotification", input);
                                editor.apply();


                            }
                        }
                    }
                }
            }
        }
    }
}
