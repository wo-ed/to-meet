package st.alker.tomeet.rest.entities;

import android.util.Log;
import com.google.gson.annotations.SerializedName;
import st.alker.tomeet.Globals;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class LoginResponse {
    private String TAG = "LoginResponse";

    @SerializedName("userName")
    private String username;
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("token_type")
    private String tokenType;
    @SerializedName(".expires")
    private String expirationDateString;

    public String getUsername() {
        return username;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public String getExpirationDateString() {
        return expirationDateString;
    }

    public Date getExpirationDate() throws ParseException {
        Calendar expirationDate = Calendar.getInstance();
        expirationDate.setTime(Globals.SERVER_TOKEN_DATE_FORMAT.parse(getExpirationDateString()));
        return expirationDate.getTime();
    }

    public boolean isValid() {
        return getUsername() != null
                && getAccessToken() != null
                && getTokenType() != null
                && getExpirationDateString() != null;
    }

    public boolean isExpired() {
        Calendar now = Calendar.getInstance();
        Calendar expirationDate = Calendar.getInstance(Globals.TIMEZONE_SERVER, Globals.LOCALE_SERVER);
        try {
            expirationDate.setTime(getExpirationDate());
        } catch (ParseException e) {
            Log.e(TAG, "expiration date could not be parsed", e);
            return true;
        }

        return now.after(expirationDate);
    }
}
