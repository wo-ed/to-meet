package st.alker.tomeet.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import st.alker.tomeet.R;
import st.alker.tomeet.activities.AddFriendActivity;
import st.alker.tomeet.rest.entities.Device;
import st.alker.tomeet.utils.BluetoothUtil;

public class FriendsAdapter extends DataAdapter<FriendsAdapter.Friend, FriendsAdapter.ViewHolder> {
    private static String TAG = "TrackingPermAdapter";
    private Context context;
    private InteractionListener interactionListener;
    private OnClickListener clickListener;

    public FriendsAdapter(Context context, InteractionListener interactionListener, OnClickListener onClickListener) {
        this.context = context;
        this.interactionListener = interactionListener;
        this.clickListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_friend, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Friend friend = items.get(position);

        if (!friend.isAdded()) {
            holder.textUsername.setText(friend.getUserName());
            holder.textMac.setText(R.string.add_this_person);
            holder.buttonAdd.setVisibility(View.VISIBLE);
            holder.bluetoothButton.setVisibility(View.GONE);
            holder.gpsButton.setVisibility(View.GONE);

            holder.buttonAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, AddFriendActivity.class);
                    intent.putExtra(AddFriendActivity.INTENT_KEY_NAME, friend.getUserName());
                    context.startActivity(intent);
                }
            });
            return;
        }
        holder.buttonAdd.setVisibility(View.GONE);

        holder.textUsername.setText(friend.getUserName());
        String mac = null;
        if (!friend.isMacPermissionGranted()) {
            mac = context.getString(R.string.no_permissions);
            holder.bluetoothButton.setVisibility(View.INVISIBLE);
        } else {
            mac = friend.getDevice().getMacBluetooth();
            if (mac == null || "".equals(mac) || BluetoothUtil.DEFAULT_MAC.equals(mac)) {
                mac = context.getString(R.string.no_bluetooth_mac);
                holder.bluetoothButton.setVisibility(View.INVISIBLE);
            } else {
                holder.bluetoothButton.setVisibility(View.VISIBLE);
                holder.bluetoothButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        interactionListener.showDeviceBluetooth(friend.getDevice());
                    }
                });
            }
        }
        if (friend.isGpsPermissionGranted()) {
            holder.gpsButton.setVisibility(View.VISIBLE);
            holder.gpsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        interactionListener.showDeviceOnMap(friend.getDevice());
                    } catch (NumberFormatException e) {
                        Log.e(TAG, "error parsing location", e);
                    }
                }
            });
        } else {
            holder.gpsButton.setVisibility(View.GONE);
        }
        holder.textMac.setText(mac);

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View view) {
                return clickListener.onLongClick(view, friend.getPermissionId(), holder.getAdapterPosition());
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textUsername;
        private TextView textMac;
        private ImageButton gpsButton;
        private ImageButton bluetoothButton;
        private ImageButton buttonAdd;

        ViewHolder(View view) {
            super(view);
            textUsername = (TextView) view.findViewById(R.id.friend_username);
            textMac = (TextView) view.findViewById(R.id.friend_mac);
            gpsButton = (ImageButton) view.findViewById(R.id.button_track_gps);
            bluetoothButton = (ImageButton) view.findViewById(R.id.button_track_bluetooth);
            buttonAdd = (ImageButton) view.findViewById(R.id.button_add_friend);
        }
    }

    public interface InteractionListener {
        void showDeviceOnMap(Device device);
        void showDeviceBluetooth(Device device);
    }

    public interface OnClickListener {
        boolean onLongClick(View view, int permissionId, int position);
    }

    public static class Friend implements Comparable<Friend>{
        private int permissionId;
        private String userName;
        private Device device;
        private boolean gpsPermissionGranted;
        private boolean macPermissionGranted;
        private boolean added;

        public Friend() {

        }

        public int getPermissionId() {
            return permissionId;
        }

        public void setPermissionId(int permissionId) {
            this.permissionId = permissionId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public Device getDevice() {
            return device;
        }

        public void setDevice(Device device) {
            this.device = device;
        }

        public boolean isGpsPermissionGranted() {
            return gpsPermissionGranted;
        }

        public void setGpsPermissionGranted(boolean gpsPermissionGranted) {
            this.gpsPermissionGranted = gpsPermissionGranted;
        }

        public boolean isMacPermissionGranted() {
            return macPermissionGranted;
        }

        public void setMacPermissionGranted(boolean macPermissionGranted) {
            this.macPermissionGranted = macPermissionGranted;
        }

        public boolean isAdded() {
            return added;
        }

        public void setAdded(boolean added) {
            this.added = added;
        }

        @Override
        public int compareTo(Friend friend) {
            if(isAdded() && !friend.isAdded()) {
                return -1;
            }
            if(!isAdded() && friend.isAdded()) {
                return 1;
            }

            if(isGpsPermissionGranted() && !friend.isGpsPermissionGranted()) {
                return -1;
            }
            if(!isGpsPermissionGranted() && friend.isGpsPermissionGranted()) {
                return 1;
            }

            if(isMacPermissionGranted() && !friend.isMacPermissionGranted()) {
                return -1;
            }
            if(!isMacPermissionGranted() && friend.isMacPermissionGranted()) {
                return 1;
            }

            return getUserName().compareTo(friend.getUserName());
        }
    }
}
