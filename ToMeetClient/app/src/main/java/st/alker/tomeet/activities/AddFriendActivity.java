package st.alker.tomeet.activities;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.loopj.android.http.RequestHandle;
import cz.msebera.android.httpclient.Header;
import st.alker.tomeet.Globals;
import st.alker.tomeet.R;
import st.alker.tomeet.alarm.AlarmService;
import st.alker.tomeet.rest.Constants;
import st.alker.tomeet.rest.GsonHttpResponseHandler;
import st.alker.tomeet.rest.RestHelper;
import st.alker.tomeet.rest.entities.LoginResponse;
import st.alker.tomeet.rest.entities.TrackingPermission;
import st.alker.tomeet.utils.BluetoothUtil;

import java.lang.ref.WeakReference;

public class AddFriendActivity extends AppCompatActivity {
    public static final String INTENT_KEY_NAME = "username";

    private ImageButton save;

    private static final String TAG = "FriendTag";

    private RestHelper restHelper;
    private RequestHandle handle;
    private TrackingPermissionPostHandler postHandler;

    private TextView friend_Name;

    private ProgressBar progressBar;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        save = (ImageButton) findViewById(R.id.save_button);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        friend_Name = (TextView)findViewById(R.id.friendName);
        if(getIntent().hasExtra(INTENT_KEY_NAME)) {
            friend_Name.setText(getIntent().getStringExtra(INTENT_KEY_NAME));
        }

        final String macAddress = BluetoothUtil.getBluetoothMacAddress(getContentResolver());

        postHandler = new AddFriendActivity.TrackingPermissionPostHandler(this);
        LoginResponse login = Globals.ENCRYPTED_STORAGE.get(getString(R.string.storage_key_login_data));
        restHelper = new RestHelper(this, login);

        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                if(macAddress == null) {
                    //Toast.makeText(AddEventActivity.this, getString(R.string.bluetooth_needed), Toast.LENGTH_SHORT).
                }

                if ("".equals(friend_Name.getText().toString())) {
                    Toast.makeText(AddFriendActivity.this, getString(R.string.missing_fields), Toast.LENGTH_SHORT).show();
                    return;
                }

                TrackingPermission trackingPermission = new TrackingPermission();
                trackingPermission.setUserName(friend_Name.getText().toString());
                trackingPermission.setMacBluetooth(macAddress);
                trackingPermission.setGpsPermissionGranted(true);
                trackingPermission.setMacPermissionGranted(true);

                if(handle == null || handle.isCancelled() || handle.isFinished()) {
                    progressBar.setVisibility(View.VISIBLE);
                    handle = restHelper.postObjectAuthorized(Constants.Urls.TRACKING_PERMISSIONS, trackingPermission, postHandler);
                }
            }

        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handle != null) {
            progressBar.setVisibility(View.GONE);
            handle.cancel(true);
        }
    }

    private static final class TrackingPermissionPostHandler extends GsonHttpResponseHandler<TrackingPermission> {
        private WeakReference<AddFriendActivity> activity;

        TrackingPermissionPostHandler(AddFriendActivity activity) {
            super(TrackingPermission.class, Globals.GSON);
            this.activity = new WeakReference<>(activity);
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "No permission added", throwable);
            AddFriendActivity activity = this.activity.get();
            if (activity != null) {
                Toast.makeText(activity, String.format(activity.getString(R.string.error_status_code), statusCode), Toast.LENGTH_SHORT).show();
                activity.progressBar.setVisibility(View.GONE);
            }
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, TrackingPermission trackingPermission) {
            Log.i(TAG, "Friend created");
            finishActivitySuccessful(trackingPermission);
        }

        @Override
        public void onSuccessParsingFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "response could not be parsed", throwable);
            finishActivitySuccessful(null);
        }

        private void finishActivitySuccessful(@Nullable TrackingPermission trackingPermission) {
            AddFriendActivity activity = this.activity.get();
            if (activity != null) {
                String name = "";
                if (trackingPermission != null) {
                    name = trackingPermission.getUserName();
                }
                Toast.makeText(activity, String.format(activity.getString(R.string.friend_saved), name), Toast.LENGTH_SHORT).show();
                activity.finish();

                // Update map markers
                Intent alarmIntent = new Intent(activity, AlarmService.class);
                alarmIntent.putExtra("type", "getFriends");
                activity.startService(alarmIntent);
            }
        }
    }
}
