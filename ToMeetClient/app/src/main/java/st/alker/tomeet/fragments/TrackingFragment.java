package st.alker.tomeet.fragments;


import android.Manifest;
import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.FrameLayout;
import android.widget.TextView;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import st.alker.tomeet.Globals;
import st.alker.tomeet.R;
import st.alker.tomeet.activities.AddEventActivity;
import st.alker.tomeet.rest.entities.Device;
import st.alker.tomeet.rest.entities.Event;
import st.alker.tomeet.rest.entities.LoginResponse;
import st.alker.tomeet.utils.LocationReceiver;

import static android.content.Context.MODE_PRIVATE;

public class TrackingFragment extends Fragment implements OnMapReadyCallback {
    private static final int REQUEST_PERMISSIONS_LOCATION = 0;
    private static final String TAG = "TrackingFragment";

    private static final String PREF_KEY_LAT = "LAT";
    private static final String PREF_KEY_LNG = "LNG";
    private static final String PREF_KEY_ZOOM = "ZOOM";
    private static final String PREF_KEY_TILT = "TILT";
    private static final String PREF_KEY_BEARING = "BEARING";

    private GoogleMap map;
    private ViewGroup infoView;
    private Marker selectedMarker;

    private Event event;
    private Device device;

    private FrameLayout containerInfo;
    BroadcastReceiver updateUIBCR = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra("type");
            addMarkers();
            selectEventOrDeviceMarker();
        }
    };

    private void selectMarker(Event event) {
        for (Marker marker : markers) {
            if (marker.getTag() instanceof Event) {
                Event markerEvent = (Event) marker.getTag();
                if (markerEvent.getId() == event.getId()) {
                    selectMarker(marker);
                    break;
                }
            }
        }
    }

    private void selectMarker(Device device) {
        for (Marker marker : markers) {
            if (marker.getTag() instanceof Device) {
                Device markerDevice = (Device) marker.getTag();
                if (markerDevice.getId() == device.getId()) {
                    selectMarker(marker);
                    break;
                }
            }
        }
    }

    private TextView textEventDate;
    private TextView textEventName;
    private TextView textEventDescription;
    private TextView textEventHost;

    private DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.GERMANY);
    private TextView textEventLocation;
    private int lastMapPaddingTop;
    private SharedPreferences defaultPrefs;

    private ArrayList<Marker> markers;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        defaultPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        markers = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tracking, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment;
        if (savedInstanceState == null) {
            mapFragment = new SupportMapFragment();
            getChildFragmentManager().beginTransaction().replace(R.id.map_placeholder, mapFragment).commit();
        } else {
            mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_placeholder);
        }
        mapFragment.getMapAsync(this);
        getActivity().registerReceiver(this.updateUIBCR, new IntentFilter("updateUIMap"));

        containerInfo = (FrameLayout) view.findViewById(R.id.container_info);
        infoView = (ViewGroup) LayoutInflater.from(getContext()).inflate(R.layout.list_item_event, containerInfo, false);
        containerInfo.addView(infoView);

        containerInfo.findViewById(R.id.button_event_location).setVisibility(View.GONE);
        textEventName = (TextView) containerInfo.findViewById(R.id.text_event_name);
        textEventDate = (TextView) containerInfo.findViewById(R.id.text_event_date);
        textEventDescription = (TextView) containerInfo.findViewById(R.id.text_event_description);
        textEventHost = (TextView) containerInfo.findViewById(R.id.text_event_host);
        textEventLocation = (TextView) containerInfo.findViewById(R.id.text_event_location);

        infoView.setTranslationY(-1000);
        infoView.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.selector_clickable_transparent));
        infoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startEventActivityFromMarker(selectedMarker);
            }
        });
    }

    private void startEventActivityFromMarker(Marker marker) {
        if (marker != null && marker.getTag() instanceof Event) {
            Event event = (Event) selectedMarker.getTag();
            Intent intent = new Intent(getContext(), AddEventActivity.class);
            intent.putExtra(AddEventActivity.INTENT_KEY_EVENT, event);
            startActivity(intent);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.map = map;
        requestPermissions();

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                selectMarker(marker);
                return false;
            }
        });

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                selectedMarker = null;
                setEvent(null);
                setDevice(null);
                setDescriptionVisible(false);
            }
        });

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                startEventActivityFromMarker(marker);
            }
        });

        addMarkers();

        restoreLastCameraPosition(map);
        selectEventOrDeviceMarker();
        zoomToEventOrDevice();
    }

    private void zoomToEventOrDevice() {
        if(event != null) {
            zoomToPoint(event.getLatitude(), event.getLongitude());
        } else if (device != null) {
            zoomToPoint(device.getLastLatitude(), device.getLastLongitude());
        }
    }

    private void selectEventOrDeviceMarker() {
        if(event != null) {
            selectMarker(event);
        } else if (device != null) {
            selectMarker(device);
        }
    }

    private void selectMarker(Marker marker) {
        marker.showInfoWindow();
        selectedMarker = marker;
        if(marker.getTag() instanceof Event) {

            setDescriptionVisible(true);

            Event event = (Event) marker.getTag();
            textEventName.setText(event.getName());
            String date = dateFormat.format(event.getDate());
            textEventDate.setText(String.format(getContext().getString(R.string.time_and_date), date));
            if(event.getDescription() == null || event.getDescription().equals("")) {
                textEventDescription.setText(R.string.no_description);
            } else {
                textEventDescription.setText(event.getDescription());
            }
            textEventHost.setText(String.format(getContext().getString(R.string.host), event.getHostUserName()));

            double lat = 0.0;
            double lng = 0.0;
            try {
                lat = Location.convert(event.getLatitude());
            } catch (Exception e) {
                Log.e(TAG, String.format("Converting latitude of Event %s failed", event.getId()), e);
            }
            try {
                lng = Location.convert(event.getLongitude());
            } catch (Exception e) {
                Log.e(TAG, String.format("Converting latitude of Event %s failed", event.getId()), e);
            }
            textEventLocation.setText(String.format(getContext().getString(R.string.location), lat, lng));
            setEvent(event);
        } else {
            setDescriptionVisible(false);
            if(marker.getTag() instanceof Device) {
                Device device = (Device)marker.getTag();
                setDevice(device);
            } else {
                setEvent(null);
                setDevice(null);
            }
        }
    }

    private void restoreLastCameraPosition(GoogleMap map) {
        double lat = defaultPrefs.getFloat(PREF_KEY_LAT, 0.0f);
        double lng = defaultPrefs.getFloat(PREF_KEY_LNG, 0.0f);
        float bearing = defaultPrefs.getFloat(PREF_KEY_BEARING, 0.0f);
        float tilt = defaultPrefs.getFloat(PREF_KEY_TILT, 0.0f);
        float zoom = defaultPrefs.getFloat(PREF_KEY_ZOOM, 0.0f);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, lng))
                .zoom(zoom)
                .tilt(tilt)
                .bearing(bearing)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        map.moveCamera(cameraUpdate);
    }

    private void setDescriptionVisible(boolean shouldBeVisible) {
        long duration = 350L;
        float y = shouldBeVisible ? 0.0f : -infoView.getHeight();
        infoView.animate().translationY(y).setDuration(duration).start();
        int paddingTop = infoView.getHeight();

        ValueAnimator animation = null;
        if(shouldBeVisible && paddingTop != lastMapPaddingTop) {
            animation = ValueAnimator.ofInt(lastMapPaddingTop, paddingTop);
            lastMapPaddingTop = paddingTop;
        } else if (!shouldBeVisible && 0 != lastMapPaddingTop){
            animation = ValueAnimator.ofInt(lastMapPaddingTop, 0);
            lastMapPaddingTop = 0;
        }

        if(animation != null) {
            animation.setDuration(duration);
            animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    map.setPadding(0, Integer.parseInt(valueAnimator.getAnimatedValue().toString()), 0, 0);
                }
            });
            animation.start();
        }
    }

    private void requestPermissions() {
        requestPermissions(
                new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                },
                REQUEST_PERMISSIONS_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (permissions.length > 0) {
            if (requestCode == REQUEST_PERMISSIONS_LOCATION
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    || grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                try {
                    map.setMyLocationEnabled(true);
                } catch (SecurityException ex) {
                    Log.e(TAG, "my location could not be enabled", ex);
                }
            }
        }
    }

    private void addMarkers() {
        selectedMarker = null;

        markers.clear();
        map.clear(); //remove all Markers before adding new ones
        String friendLocations;
        synchronized (LocationReceiver.friendSynchronizer) {
            SharedPreferences sharedPrefs = getContext().getSharedPreferences("friendLocationData", MODE_PRIVATE);
            friendLocations = sharedPrefs.getString("friendLocations", "Abort...");
        }
        if (friendLocations.equals("Abort...")) {
            Log.e(TAG, "Aborted...friendLocations was not in sharedPrefs/we don't have any friends yet");
        } else {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<Device>>() {
            }.getType();
            ArrayList<Device> friendLocationList = gson.fromJson(friendLocations, type);

            for (Device device : friendLocationList) {
                float color;
                LoginResponse login = Globals.ENCRYPTED_STORAGE.get(getString(R.string.storage_key_login_data));
                if (device.getUserName().equals(login.getUsername())) {
                    color = BitmapDescriptorFactory.HUE_GREEN;
                } else {
                    color = BitmapDescriptorFactory.HUE_AZURE;
                }
                try {
                    Marker marker = map.addMarker(new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(device.getLastLatitude()), Double.parseDouble(device.getLastLongitude())))
                            .icon(BitmapDescriptorFactory.defaultMarker(color))
                            .title(device.getUserName()));
                    marker.setTag(device);
                    markers.add(marker);
                } catch (NumberFormatException e) {
                    Log.e(TAG, "Coordinates are faulty...");
                    continue;
                }
            }
        }

        /**
         * Events
         */
        String eventLocations;
        synchronized (LocationReceiver.eventSynchronizer) {
            SharedPreferences prefs = getContext().getSharedPreferences("eventLocationData", MODE_PRIVATE);
            eventLocations = prefs.getString("eventLocations", "Abort...");
        }
        if (eventLocations.equals("Abort...")) {
            Log.e(TAG, "Aborted...eventLocations was not in sharedPrefs/we don't have any events yet");
        } else {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<Event>>() {}.getType();
            ArrayList<Event> eventLocationList = gson.fromJson(eventLocations, type);
            long day = -(24 * 60 * 60 * 1000); //24h*60m*60s*1000ms
            for (Event event : eventLocationList) {

                // event Time - current Time. If < day, event has already happened, is older than 24h, and should not be displayed.
                if ((event.getDate().getTime() - new Date().getTime()) < day) {
                    continue;
                }

                try {
                    Marker marker = map.addMarker(new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(event.getLatitude()), Double.parseDouble(event.getLongitude())))
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                            .title(event.getName()));
                    marker.setTag(event);
                    markers.add(marker);

                    map.addCircle(new CircleOptions()
                            .center(new LatLng(Double.parseDouble(event.getLatitude()), Double.parseDouble(event.getLongitude())))
                            .radius(150)
                            .strokeColor(Color.argb(75, 255, 165, 0))
                            .fillColor(Color.argb(75, 255, 165, 0)));

                } catch (NumberFormatException e) {
                    Log.e(TAG, "Coordinates are faulty...");
                    continue;
                }
            }
        }
    }

    private void zoomToPoint(String sLat, String sLng) {
        boolean success = false;
        double lat = 0.0;
        double lng = 0.0;

        try {
            lat = Location.convert(sLat);
            lng = Location.convert(sLng);
            success = true;
        } catch (Exception e) {
            Log.e(TAG, "Location could not be converted", e);
        }
        if(success) {
            zoomToPoint(lat, lng);
        }
    }

    public void zoomToPoint(Double latitude, Double longitude) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude,longitude))
                .zoom(14)
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(updateUIBCR);

        if(map != null) {
            CameraPosition cameraPosition = map.getCameraPosition();
            defaultPrefs.edit()
                    .putFloat(PREF_KEY_LAT, (float) cameraPosition.target.latitude)
                    .putFloat(PREF_KEY_LNG, (float) cameraPosition.target.longitude)
                    .putFloat(PREF_KEY_ZOOM, cameraPosition.zoom)
                    .putFloat(PREF_KEY_TILT, cameraPosition.tilt)
                    .putFloat(PREF_KEY_BEARING, cameraPosition.bearing)
                    .apply();
        }
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
        this.device = null;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
        this.event = null;
    }
}
