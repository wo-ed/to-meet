package st.alker.tomeet.rest;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.loopj.android.http.TextHttpResponseHandler;
import cz.msebera.android.httpclient.Header;

public abstract class GsonHttpResponseHandler<T> extends TextHttpResponseHandler {
    private Gson gson;
    private Class<T> clazz;

    public GsonHttpResponseHandler(Class<T> clazz) {
        this(clazz, new Gson());
    }

    public GsonHttpResponseHandler(Class<T> clazz, Gson gson) {
        this.clazz = clazz;
        this.gson = gson;
    }

    @Override
    public final void onSuccess(int statusCode, Header[] headers, String responseString) {
        T t;
        try {
            t = gson.fromJson(responseString, clazz);
        } catch(Exception ex) {
            onSuccessParsingFailure(statusCode, headers, responseString, ex);
            return;
        }
        onSuccess(statusCode, headers, t);
    }

    public abstract void onSuccess(int statusCode, Header[] headers, T t);

    public abstract void onSuccessParsingFailure(int statusCode, Header[] headers, String responseString, Throwable throwable);

    public Gson getGson() {
        return gson;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }
}
