package st.alker.tomeet.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;

import st.alker.tomeet.rest.entities.Device;
import st.alker.tomeet.rest.entities.Event;

import static android.content.Context.MODE_PRIVATE;

public class LocationReceiver extends BroadcastReceiver {

    public static final Object eventSynchronizer = new Object();
    public static final Object friendSynchronizer = new Object();
    private static final String TAG = "LocationReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        String type = intent.getStringExtra("type");

        if (type.equals("friends")) {
            ArrayList<Device> friendLocations = (ArrayList<Device>) intent.getSerializableExtra("friendList");
            addFriendLocations(context, friendLocations);
        } else if (type.equals("events")) {
            ArrayList<Event> eventLocations = (ArrayList<Event>) intent.getSerializableExtra("eventList");
            addEventLocations(context, eventLocations);
        } else if (type.equals("zoom")) {
            try {
                Thread.sleep(500); //short wait for Trackingfragment to load and register broadcast receiver
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Intent newIntent = new Intent();
            newIntent.setAction("updateUIMap");
            newIntent.putExtra("latitude", intent.getDoubleExtra("latitude", 0.0));
            newIntent.putExtra("longitude", intent.getDoubleExtra("longitude", 0.0));
            newIntent.putExtra("type", intent.getStringExtra("type"));
            context.sendBroadcast(newIntent);
        } else {
            Log.e(TAG, "received wrong type - aborted");
        }

    }

    /**
     * Save list to sharedPrefs, notify the map fragment
     *
     * @param context
     * @param locations
     */
    private void addEventLocations(Context context, ArrayList<Event> locations) {
        synchronized (LocationReceiver.eventSynchronizer) {
            SharedPreferences sharedPrefs = context.getSharedPreferences("eventLocationData", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPrefs.edit();
            Gson gson = new Gson();
            String input = gson.toJson(locations);
            editor.remove("eventLocations");
            editor.commit();
            editor.putString("eventLocations", input);
            editor.apply();
        }
        Intent i = new Intent();
        i.putExtra("type", "eventLocations");//irrelevant, but needed
        i.setAction("updateUIMap");
        context.sendBroadcast(i);
    }

    /**
     * Save friend locations in sharedPreferences
     *
     * @ArrayList<Device> friendLocations
     */
    public void addFriendLocations(Context context, ArrayList<Device> friendLocationsList) {
        synchronized (LocationReceiver.friendSynchronizer) {
            SharedPreferences sharedPrefs = context.getSharedPreferences("friendLocationData", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPrefs.edit();

            Gson gson = new Gson();
            String friendLocations = gson.toJson(friendLocationsList);
            editor.remove("friendLocations"); //remove old Locations
            editor.commit();
            editor.putString("friendLocations", friendLocations);
            editor.apply();
        }
        Intent i = new Intent();
        i.putExtra("type", "friendLocations");//irrelevant, but needed
        i.setAction("updateUIMap");
        context.sendBroadcast(i);

    }
}
