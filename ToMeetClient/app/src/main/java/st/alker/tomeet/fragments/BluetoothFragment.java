package st.alker.tomeet.fragments;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;


import com.cardiomood.android.controls.gauge.SpeedometerGauge;
import st.alker.tomeet.R;

/**
 * A simple {@link Fragment} subclass.
 */

public class BluetoothFragment extends Fragment {

    private Button stopScanning;
    private Button startScanning;
    private TextView showDistanceInMeter;
    private TextView header;

    BluetoothAdapter mBluetoothAdapter;
    BroadcastReceiver mBroadcastReceiver;
    SpeedometerGauge mSpeedometerGauge;
    ProgressBar mProgressBar;

    // TODO: What if the fragment was opened by the NavigationDrawer? Maybe show a list of available devices or get the last mac from SharedPreferences
    // Test-Bluetooth Address

    private String macAddress = "48:74:6E:56:02:8E";
    private boolean cancelled = false;

    public BluetoothFragment() {
        // Required empty public constructor
    }

    private double calculateDistance(int rssi) {
        double txPower = -59;
        double distance = Math.pow(10, ((txPower - rssi) / 20));
        distance = Math.round(100.0 * distance) / 100.0;
        return distance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Vibrator mVibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        } catch (NullPointerException e) {
            showUnsupportedMessage();
        }

        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String action = intent.getAction();

                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    Log.i("Bluetooth Adapter", "Devices found");
                    final int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);

                    BluetoothDevice mBluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                    String foundAdress = mBluetoothDevice.getAddress();
                    String foundName = mBluetoothDevice.getName();

                    if (foundAdress.equals(macAddress)) {

                        Log.i("Bluetooth Adapter", "Your friend " + foundName + " is within Bluetooth reach");

                        showDistanceInMeter.setText(String.format("%s Meter", String.valueOf(calculateDistance(rssi))));
                        header.setText(String.format(getString(R.string.bluetooth_distance), foundName));
                        mSpeedometerGauge.setSpeed(Math.abs(calculateDistance(rssi)), true);

                        mVibrator.vibrate(50);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(3000);
                                } catch (InterruptedException e) {
                                    Log.e("Thread", "Thread.sleep() was interrupted", e);
                                }
                                mBluetoothAdapter.cancelDiscovery();
                            }
                        }).start();
                        // Results in startDiscovery because cancelled equals false
                        // Remove this line if you want to scan less often.

                    }
                } else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                    if(!cancelled) {
                        mBluetoothAdapter.startDiscovery();
                    }
                }
            }
        };
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        FrameLayout mFrameLayout = (FrameLayout) inflater.inflate(R.layout.fragment_bluetooth, container, false);

        final Vibrator mVibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        mSpeedometerGauge = (SpeedometerGauge) mFrameLayout.findViewById(R.id.speedometer);
        mSpeedometerGauge.setSpeed(mSpeedometerGauge.getMaxSpeed(), false);
        mSpeedometerGauge.setLabelConverter(new SpeedometerGauge.LabelConverter() {

            @Override
            public String getLabelFor(double progress, double maxProgress) {
                return String.valueOf((int) Math.round(progress));
            }
        });

        mSpeedometerGauge.setMaxSpeed(20);
        mSpeedometerGauge.setMajorTickStep(2);
        mSpeedometerGauge.setMinorTicks(2);
        mSpeedometerGauge.addColoredRange(0, 7, Color.GREEN);
        mSpeedometerGauge.addColoredRange(7, 14, Color.YELLOW);
        mSpeedometerGauge.addColoredRange(14, 20, Color.RED);
        mSpeedometerGauge.setLabelTextSize(25);

        stopScanning = (Button) mFrameLayout.findViewById(R.id.stopScanning);
        startScanning = (Button) mFrameLayout.findViewById(R.id.startScanning);
        showDistanceInMeter = (TextView) mFrameLayout.findViewById(R.id.showDistanceInMeter);
        header = (TextView) mFrameLayout.findViewById(R.id.header);
        mProgressBar = (ProgressBar) mFrameLayout.findViewById(R.id.mProgressBar);

//        int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
//        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.
//                ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);

        stopScanning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVibrator.vibrate(1);
                stopScanning();
            }
        });

        startScanning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mVibrator.vibrate(1);
                startScanning();
            }
        });

        startScanning();
        return mFrameLayout;
    }

    private void stopScanning() {
        if(mBluetoothAdapter != null) {
            cancelled = true;
            mBluetoothAdapter.cancelDiscovery();
            Log.i("Bluetooth Adapter", "Discovery canceled");
            mProgressBar.setVisibility(View.INVISIBLE);
            stopScanning.setVisibility(View.GONE);
            startScanning.setVisibility(View.VISIBLE);
        } else {
            showUnsupportedMessage();
        }
    }
    private void startScanning() {
        if(mBluetoothAdapter != null) {
            cancelled = false;
            mBluetoothAdapter.startDiscovery();
            Log.i("Bluetooth Adapter", "Started scanning for devices");
            mProgressBar.setVisibility(View.VISIBLE);
            stopScanning.setVisibility(View.VISIBLE);
            startScanning.setVisibility(View.GONE);
        } else {
            showUnsupportedMessage();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null){
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
            getActivity().unregisterReceiver(mBroadcastReceiver);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getActivity() != null) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        registerReceiver();
    }

    private void registerReceiver() {
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        getActivity().registerReceiver(mBroadcastReceiver, filter);
    }

    private void showUnsupportedMessage() {
        Log.e("Bluetooth Adapter", getString(R.string.bluetooth_not_supported));
        Toast.makeText(getContext(), getString(R.string.bluetooth_not_supported), Toast.LENGTH_SHORT).show();
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
}