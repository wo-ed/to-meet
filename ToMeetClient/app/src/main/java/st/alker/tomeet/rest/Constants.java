package st.alker.tomeet.rest;

public class Constants {
    public static final class Urls {
        public static final String BASE = "https://tomeet.azurewebsites.net";
        public static final String API = BASE + "/api";

        public static final String TOKEN = BASE + "/Token";
        public static final String ACCOUNT = API + "/Account";
        public static final String REGISTER = ACCOUNT + "/Register";
        public static final String DEVICES = API + "/Devices";
        public static final String TRACKING_PERMISSIONS = API + "/TrackingPermissions";
        public static final String TRACKING_REQUESTS = API + "/TrackingRequests";
        public static final String EVENTS = API + "/Events";
        public static final String INVITATIONS = API + "/Invitations";
        public static final String ARRIVAL = EVENTS + "/Arrival";
    }

    public static final String AUTHORIZATION = "Authorization";
    public static final String CONTENT_TYPE = "ContentType";
    public static final String ACCEPT = "Accept";

    public static final class MimeTypes {
        public static final String JSON = "application/json";
        public static final String X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";
    }

    public static final String GRANT_TYPE = "grant_type";
    public static final String GRANT_TYPE_PASSWORD = "password";
}
