package st.alker.tomeet.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.github.fabtransitionactivity.SheetLayout;
import com.loopj.android.http.RequestHandle;
import cz.msebera.android.httpclient.Header;
import st.alker.tomeet.Globals;
import st.alker.tomeet.R;
import st.alker.tomeet.activities.AddEventActivity;
import st.alker.tomeet.adapters.EventsAdapter;
import st.alker.tomeet.geofence.GeoService;
import st.alker.tomeet.rest.Constants;
import st.alker.tomeet.rest.GsonHttpResponseHandler;
import st.alker.tomeet.rest.RestHelper;
import st.alker.tomeet.rest.entities.Event;
import st.alker.tomeet.rest.entities.LoginResponse;

import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class EventsFragment extends Fragment {
    private static final int REQUEST_ADD_EVENT = 0;

    private EventsAdapter adapter;
    private EventsAdapter.InteractionListener listener;
    private ProgressBar progressBar;
    private RestHelper restHelper;
    private RequestHandle handle;
    private EventsGetHandler handler;
    private SwipeRefreshLayout swipeContainer;
    private FloatingActionButton fab;
    private SheetLayout sheetLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new EventsAdapter(getContext(), listener);
        LoginResponse login = Globals.ENCRYPTED_STORAGE.get(getString(R.string.storage_key_login_data));
        restHelper = new RestHelper(getContext(), login);
        handler = new EventsGetHandler(this);
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        final RecyclerView listEvents = (RecyclerView) view.findViewById(R.id.list_events);
        listEvents.setAdapter(adapter);


        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listEvents.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider));
        listEvents.addItemDecoration(dividerItemDecoration);

        listEvents.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx,int dy){
                super.onScrolled(recyclerView, dx, dy);

                if (dy >0) {
                    // Scroll Down
                    if (fab.isShown()) {
                        fab.hide();
                    }
                }else if (dy <0) {
                    // Scroll Up
                    if (!fab.isShown()) {
                        fab.show();
                    }
                }
            }
        });

        fab = (FloatingActionButton) view.findViewById(R.id.fab_add_event);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sheetLayout.setVisibility(View.VISIBLE);
                sheetLayout.expandFab();
            }
        });

        sheetLayout = (SheetLayout) view.findViewById(R.id.bottom_sheet);
        sheetLayout.setFab(fab);
        sheetLayout.setFabAnimationEndListener(new SheetLayout.OnFabAnimationEndListener() {
            @Override
            public void onFabAnimationEnd() {
                Intent intent = new Intent(getContext(), AddEventActivity.class);
                startActivityForResult(intent, REQUEST_ADD_EVENT);
            }
        });

        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeContainer.setColorSchemeColors(
                ContextCompat.getColor(getContext(), R.color.colorAccent),
                ContextCompat.getColor(getContext(), R.color.colorPrimary));

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                progressBar.setVisibility(View.GONE);
                handle.cancel(true);
                handle = restHelper.getObjectsAuthorized(Constants.Urls.EVENTS, handler);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        sheetLayout.setVisibility(View.GONE);
        fab.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.VISIBLE);
        handle = restHelper.getObjectsAuthorized(Constants.Urls.EVENTS, handler);
    }

    @Override
    public void onPause() {
        super.onPause();
        handle.cancel(true);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (EventsAdapter.InteractionListener)context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_events, container, false);
    }

    private static class EventsGetHandler extends GsonHttpResponseHandler<Event[]> {
        private static final String TAG = "EventsGetHandler";

        private WeakReference<EventsFragment> fragment;

        EventsGetHandler(EventsFragment fragment) {
            super(Event[].class, Globals.GSON);
            this.fragment = new WeakReference<>(fragment);
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, Event[] events) {
            Log.i(TAG, "Events were retrieved successfully");
            EventsFragment fragment = this.fragment.get();
            if (fragment != null) {
                ArrayList<Event> futureEvents = new ArrayList<>();
                long day = -(24 * 60 * 60 * 1000); //24h*60m*60s*1000ms
                for (Event event : events) {
                    // event Time - current Time. If < day, event has already happened, is older than 24h, and should not be displayed.
                    if ((event.getDate().getTime() - new Date().getTime()) < day) {
                        continue;
                    }
                    futureEvents.add(event);
                }
                fragment.adapter.setItems(futureEvents);
                fragment.progressBar.setVisibility(View.GONE);
                fragment.swipeContainer.setRefreshing(false);

                /**Update Map*/
                Intent container = new Intent(fragment.getContext(), GeoService.class);
                container.putExtra("type", "getEvents");
                fragment.getContext().startService(container);
            }
        }

        @Override
        public void onSuccessParsingFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "response could not be parsed", throwable);
            EventsFragment fragment = this.fragment.get();
            if (fragment != null) {
                Context ctx = fragment.getContext();
                Toast.makeText(ctx, String.format(ctx.getString(R.string.internal_error_status_code), 1), Toast.LENGTH_SHORT).show();
                fragment.progressBar.setVisibility(View.GONE);
                fragment.swipeContainer.setRefreshing(false);
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Log.e(TAG, "Events could not be retrieved", throwable);
            EventsFragment fragment = this.fragment.get();
            if (fragment != null) {
                Context ctx = fragment.getContext();
                Toast.makeText(ctx, String.format(ctx.getString(R.string.error_status_code), statusCode), Toast.LENGTH_SHORT).show();
                fragment.progressBar.setVisibility(View.GONE);
                fragment.swipeContainer.setRefreshing(false);
            }
        }

    }
}
