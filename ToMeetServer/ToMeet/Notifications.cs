﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ToMeet
{
    public static class Notifications
    {
        public static async Task<string> send(string token, int resourceId, string title, string body)
        {
            try
            {
                string fcmKey = ConfigurationManager.ConnectionStrings["FCM_Server_key"].ConnectionString;

                string message = "{ \"to\" : \"" + token +
                                 "\", \"data\" : { \"message\" : \"" + body + "\", \"title\" : \"" + title + "\", \"id\" : " + resourceId + " }}";

                using (WebClient client = new WebClient())
                {
                    client.Encoding = Encoding.UTF8;
                    client.Headers.Add("Authorization", "key=" + fcmKey);
                    client.Headers.Add("Content-Type", "application/json");
                    return await client.UploadStringTaskAsync("https://fcm.googleapis.com/fcm/send", message);
                }
            }
            catch(Exception ex)
            {
                Trace.TraceInformation(ex.StackTrace);
            }
            return "";
        }
    }
}