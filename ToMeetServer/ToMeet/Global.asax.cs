﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ToMeet.Models;

namespace ToMeet
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            InitializeAutoMapper();
        }

        private void InitializeAutoMapper()
        {
            //TODO: async
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<DeviceDtoInput, DeviceInfo>();

                cfg.CreateMap<DeviceInfo, DeviceDtoOutput>().AfterMap((device, output) =>
                {
                    using (ApplicationDbContext db = new ApplicationDbContext())
                    {
                        string userName = db.Users.Single(u => u.Id == device.UserId).UserName;
                        output.UserName = userName;
                    }
                });

                cfg.CreateMap<TrackingPermissionDtoInput, TrackingPermission>().AfterMap((input, permission) =>
                {
                    using (ApplicationDbContext db = new ApplicationDbContext())
                    {
                        string userId = db.Users.Single(u => u.UserName == input.UserName).Id;
                        permission.UserId = userId;
                    }
                });
                cfg.CreateMap<TrackingPermission, TrackingPermissionDtoOutput>().AfterMap((permission, output) =>
                {
                    using (ApplicationDbContext db = new ApplicationDbContext())
                    {
                        string userName = db.Users.Single(u => u.Id == permission.UserId).UserName;
                        output.UserName = userName;
                        output.Device = Mapper.Map<DeviceDtoOutput>(permission.DeviceInfo);
                        output.DeviceId = permission.DeviceInfoId;
                    }
                });

                cfg.CreateMap<TrackingRequestDtoInput, TrackingRequest>().AfterMap((input, request) =>
                {
                    request.DeviceInfoId = input.DeviceId;
                });
                cfg.CreateMap<TrackingRequest, TrackingRequestDtoOutput>().AfterMap((request, output) =>
                {
                    output.DeviceId = request.DeviceInfoId;
                });

                cfg.CreateMap<EventDtoInput, Event>();

                cfg.CreateMap<Event, EventDtoOutput>().AfterMap((ev, output) =>
                {
                    using (ApplicationDbContext db = new ApplicationDbContext())
                    {
                        string userName = db.Users.Single(u => u.Id == ev.HostUserId).UserName;
                        output.HostUserName = userName;
                        var invitedUserIds = ev.Invitation.Select(i => i.GuestUserId).ToList();
                        var invitedUsers = db.Users.Where(u => invitedUserIds.Contains(u.Id));
                        output.InvitedUsers = invitedUsers.Select(u => u.UserName).ToList();
                    }
                });

                cfg.CreateMap<InvitationDtoInput, Invitation>().AfterMap((input, invitation) =>
                {
                    using (ApplicationDbContext db = new ApplicationDbContext())
                    {
                        string userId = db.Users.Single(u => u.UserName == input.GuestUserName).Id;
                        invitation.GuestUserId = userId;
                    }
                });
                cfg.CreateMap<Invitation, InvitationDtoOutput>().AfterMap((invitation, output) =>
                {
                    using (ApplicationDbContext db = new ApplicationDbContext())
                    {
                        string userName = db.Users.Single(u => u.Id == invitation.GuestUserId).UserName;
                        output.GuestUserName = userName;
                        output.Event = Mapper.Map<EventDtoOutput>(invitation.Event);
                    }
                });
            });
        }
    }
}