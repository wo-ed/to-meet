﻿using System;
using AutoMapper;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ToMeet.Models;

namespace ToMeet.Controllers
{
    [Authorize]
    public class DevicesController : ApiController
    {
        private readonly ToMeetEntities db = new ToMeetEntities();
        private string UserId => User.Identity.GetUserId();

        // GET: api/DeviceInfo
        /// <summary>
        /// Get all devices created by the authenticated user and all devices the user has access to. 
        /// The access to devices is managed by tracking permissions.
        /// </summary>
        public async Task<IEnumerable<DeviceDtoOutput>> GetDevice()
        {
            IEnumerable<DeviceInfo> userDevices = await GetUserDevices();
            IEnumerable<DeviceInfo> permittedDevices = await GetPermittedDevices();
            return userDevices.Union(permittedDevices).ToList().Select(Mapper.Map<DeviceDtoOutput>);
        }

        private Task<List<DeviceInfo>> GetUserDevices()
        {
            return db.DeviceInfo.Where(d => d.UserId == UserId).ToListAsync();
        }

        private async Task<IEnumerable<DeviceInfo>> GetPermittedDevices()
        {
            List<TrackingPermission> permissions = await GetTrackingPermissionsForUser();
            return permissions.Select(p => p.DeviceInfo);
        }

        private Task<List<TrackingPermission>> GetTrackingPermissionsForUser()
        {
            return db.TrackingPermission.Where(p =>
                    p.UserId == UserId
                    && p.DeviceInfo.UserId != UserId
                    && p.GPSPermissionGranted
                    || p.MACPermissionGranted)
                .ToListAsync();
        }

        // GET: api/DeviceInfo/5
        /// <summary>
        /// Get the device with the specified ID. 
        /// If the authenticated user did not create the device and has no permissions for it the request will fail.
        /// </summary>
        [ResponseType(typeof(DeviceDtoOutput))]
        public async Task<IHttpActionResult> GetDevice(int id)
        {
            DeviceInfo device = await db.DeviceInfo.FindAsync(id);
            DeviceDtoOutput output;

            if (device == null)
            {
                return NotFound();
            }
            if (device.UserId != UserId)
            {
                TrackingPermission permission = GetTrackingPermission(device);
                if (permission == null || !permission.GPSPermissionGranted && !permission.MACPermissionGranted)
                {
                    return Unauthorized();
                }
                output = Mapper.Map<DeviceDtoOutput>(GetManipulatedDevice(permission));
            }
            else
            {
                output = Mapper.Map<DeviceDtoOutput>(device);
            }

            return Ok(output);
        }

        private TrackingPermission GetTrackingPermission(DeviceInfo device)
        {
            return device.TrackingPermission.SingleOrDefault(p => p.UserId == UserId);
        }

        // PUT: api/DeviceInfo/5
        /// <summary>
        /// Edit the device with the specified ID. 
        /// If the authenticated user did not create the device the request will fail.
        /// </summary>
        [ResponseType(typeof(DeviceDtoOutput))]
        public async Task<IHttpActionResult> PutDevice(int id, DeviceDtoInput input)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DeviceInfo device = await db.DeviceInfo.FindAsync(id);
            if (device == null)
            {
                return NotFound();
            }
            if (device.UserId != UserId)
            {
                return Unauthorized();
            }

            DeviceInfo duplicate = await GetUserDeviceAsync(input.MACBluetooth);
            if (duplicate != null && duplicate.Id != device.Id)
            {
                return Conflict();
            }

            Mapper.Map(input, device);
            db.Entry(device).State = EntityState.Modified;
            await db.SaveChangesAsync();
            await StoreToken(input.FcmToken, device);

            var output = Mapper.Map<DeviceDtoOutput>(device);
            return Ok(output);
        }

        // POST: api/DeviceInfo
        /// <summary>
        /// Create a new device for the authenticated user. 
        /// If the user has already created a device with the specified MAC adress the existing device will be edited instead.
        /// Returns 201 for creation and 200 for update.
        /// </summary>
        [ResponseType(typeof(DeviceDtoOutput))]
        public async Task<IHttpActionResult> PostDevice(DeviceDtoInput input)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            bool created = false;
            DeviceInfo device = await GetUserDeviceAsync(input.MACBluetooth);
            if (device == null)
            {
                device = Mapper.Map<DeviceInfo>(input);
                device.UserId = User.Identity.GetUserId();
                db.DeviceInfo.Add(device);
                created = true;
            }
            else
            {
                Mapper.Map(input, device);
                db.Entry(device).State = EntityState.Modified;
            }

            await db.SaveChangesAsync();
            await StoreToken(input.FcmToken, device);

            DeviceDtoOutput output = Mapper.Map<DeviceDtoOutput>(device);
            if (created)
            {
                return CreatedAtRoute("DefaultApi", new {id = device.Id}, output);
            }
            return Ok(output);
        }

        // DELETE: api/DeviceInfo/5
        /// <summary>
        /// Delete the device with the specified ID. 
        /// If the authenticated user did not create the device the request will fail.
        /// All tracking permissions and tracking requests for this device will be deleted as well.
        /// </summary>
        [ResponseType(typeof(DeviceDtoOutput))]
        public async Task<IHttpActionResult> DeleteDevice(int id)
        {
            DeviceInfo device = await db.DeviceInfo.FindAsync(id);
            if (device == null)
            {
                return NotFound();
            }
            if (device.UserId != UserId)
            {
                return Unauthorized();
            }

            db.TrackingPermission.RemoveRange(db.TrackingPermission.Where(p => p.DeviceInfoId == id));
            db.TrackingRequest.RemoveRange(db.TrackingRequest.Where(r => r.DeviceInfoId == id));
            db.DeviceInfo.Remove(device);
            await db.SaveChangesAsync();

            DeviceDtoOutput output = Mapper.Map<DeviceDtoOutput>(device);
            return Ok(output);
        }

        private Task<DeviceInfo> GetUserDeviceAsync(string mac)
        {
            return db.DeviceInfo.Where(d =>
                    d.MACBluetooth == mac
                    && d.UserId == UserId)
                .SingleOrDefaultAsync();
        }

        private static DeviceInfo GetManipulatedDevice(TrackingPermission permission)
        {
            DeviceInfo device = permission.DeviceInfo;
            if (!permission.GPSPermissionGranted)
            {
                device.LastLatitude = null;
                device.LastLongitude = null;
                device.LastDate = null;
            }
            if (!permission.MACPermissionGranted)
            {
                device.MACBluetooth = null;
            }

            return device;
        }

        private async Task StoreToken(string token, DeviceInfo device)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                return;
            }
            FCMToken dbToken = await db.FCMToken.FirstOrDefaultAsync(t => t.DeviceId == device.Id);
            if (dbToken == null)
            {
                dbToken = new FCMToken
                {
                    DeviceId = device.Id,
                    Token = token
                };
                db.FCMToken.Add(dbToken);
            }
            else
            {
                dbToken.Token = token;
                db.Entry(dbToken).State = EntityState.Modified;
            }
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}