﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace ToMeet.Controllers
{
    public static class ControllerExtensions
    {
        public static ResponseMessageResult UnprocessableEntity(this ApiController controller, string message)
        {
            return new ResponseMessageResult(
                controller.Request.CreateErrorResponse(
                    (HttpStatusCode) 422,
                    new HttpError(message)
                )
            );
        }
        public static ResponseMessageResult UnprocessableEntity(this ApiController controller)
        {
            return new ResponseMessageResult(
                controller.Request.CreateErrorResponse(
                    (HttpStatusCode) 422,
                    new HttpError()
                )
            );
        }
    }
}