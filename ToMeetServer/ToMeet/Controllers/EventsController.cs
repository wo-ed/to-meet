﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.Ajax.Utilities;
using ToMeet.Models;

namespace ToMeet.Controllers
{
    [Authorize]
    public class EventsController : ApiController
    {
        private readonly ToMeetEntities trackingEntities = new ToMeetEntities();
        private readonly ApplicationDbContext userEntities = new ApplicationDbContext();
        private string UserId => User.Identity.GetUserId();

        // GET: api/Events
        /// <summary>
        /// Get all events created by the authenticated user and all events the user is invited to.
        /// Users can by invited to events simply by creating an invitation.
        /// </summary>
        public async Task<IEnumerable<EventDtoOutput>> GetEvent()
        {
            IEnumerable<Event> userEvents = await GetUserEvents();
            IEnumerable<Event> permittedEvents = await GetPermittedEvents();
            return userEvents.Union(permittedEvents)
                .ToList()
                .OrderBy(e => e.Date)
                .Select(ev =>
                {
                    var mapped = Mapper.Map<EventDtoOutput>(ev);
                    if (ev.HostUserId != UserId)
                    {
                        mapped.InvitationId = ev.Invitation.First(inv => inv.GuestUserId == UserId).Id;
                    }
                    return mapped;
                });
        }

        private Task<List<Event>> GetUserEvents()
        {
            return trackingEntities.Event.Where(ev => ev.HostUserId == UserId).ToListAsync();
        }

        private async Task<IEnumerable<Event>> GetPermittedEvents()
        {
            List<Invitation> invitations = await GetInvitationsForUser();
            return invitations.Select(inv => inv.Event);
        }

        private Task<List<Invitation>> GetInvitationsForUser()
        {
            return trackingEntities.Invitation.Where(inv => inv.GuestUserId == UserId).ToListAsync();
        }

        private async Task<bool> IsUserInvitedAsync(Event ev)
        {
            int count = await
                trackingEntities.Invitation.Where(inv =>
                        inv.EventId == ev.Id
                        && inv.GuestUserId == UserId)
                    .CountAsync();
            return count > 0;
        }

        // GET: api/Events/5
        /// <summary>
        /// Get the event with the specified ID. 
        /// If the authenticated user did not create the event and is not invited to it the request will fail.
        /// </summary>
        [ResponseType(typeof(EventDtoOutput))]
        public async Task<IHttpActionResult> GetEvent(int id)
        {
            Event ev = await trackingEntities.Event.FindAsync(id);

            if (ev == null)
            {
                return NotFound();
            }
            if (ev.HostUserId != UserId && !await IsUserInvitedAsync(ev))
            {
                return Unauthorized();
            }
            EventDtoOutput output = Mapper.Map<EventDtoOutput>(ev);
            if (ev.HostUserId != UserId)
            {
                output.InvitationId = ev.Invitation.First(inv => inv.GuestUserId == UserId).Id;
            }

            return Ok(output);
        }

        // PUT: api/Events/5
        /// <summary>
        /// Edit the event with the specified ID. 
        /// If the authenticated user did not create the event the request will fail.
        /// </summary>
        [ResponseType(typeof(EventDtoOutput))]
        public async Task<IHttpActionResult> PutEvent(int id, EventDtoInput input)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Event ev = await trackingEntities.Event.FindAsync(id);
            if (ev == null)
            {
                return NotFound();
            }
            if (ev.HostUserId != UserId)
            {
                return Unauthorized();
            }

            Mapper.Map(input, ev);
            trackingEntities.Entry(ev).State = EntityState.Modified;

            await trackingEntities.SaveChangesAsync();

            RemoveInvitations(id);
            await AddInvitations(input, ev);
            await trackingEntities.SaveChangesAsync();

            await SendInvitationNotifications(ev, EventAction.Update);

            EventDtoOutput output = Mapper.Map<EventDtoOutput>(ev);
            return Ok(output);
        }

        [Authorize]
        [Route("api/Events/Arrival/{EventId}")]
        [HttpPost]
        public async Task<IHttpActionResult> PostArrivalNotification([FromUri]int eventId)
        {
            Event ev = await trackingEntities.Event.FindAsync(eventId);
            if (ev == null)
            {
                return NotFound();
            }
            if (ev.HostUserId != UserId && !await IsUserInvitedAsync(ev))
            {
                return Unauthorized();
            }
            string title = "Friend arrival";
            string body = User.Identity.Name + " has arrived at Event " + ev.Name;
            await SendNotificationToGuests(ev, title, body);
            if (ev.HostUserId != UserId)
            {
                await SendNotificationToUserDevices(ev, title, body, ev.HostUserId);
            }

            return Ok();
        }

        private void RemoveInvitations(int eventId)
        {
            trackingEntities.Invitation.RemoveRange(trackingEntities.Invitation.Where(i => i.EventId == eventId));
        }

        // POST: api/Events
        /// <summary>
        /// Create a new event with the authenticated user as host.
        /// Events can only be created by POST requests but not edited. Use PUT instead.
        /// </summary>
        [ResponseType(typeof(EventDtoOutput))]
        public async Task<IHttpActionResult> PostEvent(EventDtoInput input)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Event ev = Mapper.Map<Event>(input);
            ev.HostUserId = User.Identity.GetUserId();

            trackingEntities.Event.Add(ev);
            await trackingEntities.SaveChangesAsync();

            await AddInvitations(input, ev);
            await trackingEntities.SaveChangesAsync();

            await SendInvitationNotifications(ev, EventAction.Create);

            EventDtoOutput output = Mapper.Map<EventDtoOutput>(ev);
            return CreatedAtRoute("DefaultApi", new {id = ev.Id}, output);
        }

        private async Task SendInvitationNotifications(Event ev, EventAction action)
        {
            string title;
            string body;

            switch (action)
            {
                    case EventAction.Create:
                    title = "New event invitation";
                    body = "You are invited to event " + ev.Name;
                    break;
                    case EventAction.Update:
                    title = "Event updated";
                    body = "Event " + ev.Name + " has been changed";
                    break;
                    case EventAction.Delete:
                    title = "Event deleted";
                    body = "Event " + ev.Name + " has been deleted";
                    break;
                default:
                    return;
            }

            await SendNotificationToGuests(ev, title, body);
        }

        private async Task SendNotificationToGuests(Event ev, string title, string body)
        {
            foreach (Invitation invitation in ev.Invitation)
            {
                string guestUserId = invitation.GuestUserId;
                await SendNotificationToUserDevices(ev, title, body, guestUserId);
            }
        }

        private async Task SendNotificationToUserDevices(Event ev, string title, string body, string userId)
        {
            var devices = trackingEntities.DeviceInfo.Where(d => d.UserId == userId && d.UserId != UserId);
            foreach (DeviceInfo device in devices)
            {
                var token = await trackingEntities.FCMToken.FirstOrDefaultAsync(t => t.DeviceId == device.Id);
                if (token != null)
                {
                    await Notifications.send(token.Token, ev.Id, title, body);
                }
            }
        }

        private async Task AddInvitations(EventDtoInput input, Event ev)
        {
            if (input.InvitedUsers != null)
            {
                var invitedUsers = new HashSet<string>(input.InvitedUsers.Select(u => u.Trim()));

                foreach (string invitedUser in invitedUsers)
                {
                    if (invitedUser == User.Identity.Name)
                    {
                        continue;
                    }
                    ApplicationUser friend = await GetUserByName(invitedUser);
                    if (friend == null)
                    {
                        continue;
                    }

                    Invitation inv = new Invitation
                    {
                        EventId = ev.Id,
                        GuestUserId = friend.Id
                    };
                    trackingEntities.Invitation.Add(inv);
                }
            }
        }

        // DELETE: api/Events/5
        /// <summary>
        /// Delete the event with the specified ID. 
        /// If the authenticated user did not create the event the request will fail.
        /// All invitations for this event will be deleted as well.
        /// </summary>
        [ResponseType(typeof(EventDtoOutput))]
        public async Task<IHttpActionResult> DeleteEvent(int id)
        {
            Event ev = await trackingEntities.Event.FindAsync(id);
            if (ev == null)
            {
                return NotFound();
            }
            if (ev.HostUserId != UserId)
            {
                return Unauthorized();
            }

            await SendInvitationNotifications(ev, EventAction.Delete);

            RemoveInvitations(ev.Id);
            trackingEntities.Event.Remove(ev);
            await trackingEntities.SaveChangesAsync();

            EventDtoOutput output = Mapper.Map<EventDtoOutput>(ev);
            return Ok(output);
        }

        private async Task<ApplicationUser> GetUserByName(string userName)
        {
            return await userEntities.Users.SingleOrDefaultAsync(u => u.UserName == userName);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                trackingEntities.Dispose();
                userEntities.Dispose();
            }
            base.Dispose(disposing);
        }

        public enum EventAction
        {
            Create,
            Update,
            Delete
        }
    }
}