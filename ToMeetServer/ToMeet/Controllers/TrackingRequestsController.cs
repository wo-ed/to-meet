﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Security;
using AutoMapper;
using Microsoft.AspNet.Identity;
using ToMeet.Models;

namespace ToMeet.Controllers
{
    [Authorize]
    public class TrackingRequestsController : ApiController
    {
        private ToMeetEntities trackingEntities = new ToMeetEntities();
        private ApplicationDbContext userEntities = new ApplicationDbContext();

        private string UserId => User.Identity.GetUserId();


        // GET: api/TrackingRequests
        /// <summary>
        /// Get all tracking requests created by the authenticated user.
        /// </summary>
        public async Task<IEnumerable<TrackingRequestDtoOutput>> GetTrackingRequest()
        {
            var requests = await GetUserTrackingRequests().ToListAsync();
            return requests.Select(Mapper.Map<TrackingRequestDtoOutput>);
        }

        private IQueryable<TrackingRequest> GetUserTrackingRequests()
        {
            return trackingEntities.TrackingRequest.Where(p => p.UserId == UserId);
        }

        // GET: api/TrackingRequests/5
        /// <summary>
        /// Get the tracking request with the specified ID. 
        /// If the authenticated user did not create the tracking request the request will fail.
        /// </summary>
        [ResponseType(typeof(TrackingRequestDtoOutput))]
        public async Task<IHttpActionResult> GetTrackingRequest(int id)
        {
            TrackingRequest trackingRequest = await trackingEntities.TrackingRequest.FindAsync(id);
            if (trackingRequest == null)
            {
                return NotFound();
            }
            if (trackingRequest.UserId != UserId)
            {
                return Unauthorized();
            }

            return Ok(trackingRequest);
        }

        // PUT: api/TrackingRequests/5
        /// <summary>
        /// Edit the tracking request with the specified ID. 
        /// If the authenticated user did not create the tracking request the request will fail.
        /// </summary>
        [ResponseType(typeof(TrackingRequestDtoOutput))]
        public async Task<IHttpActionResult> PutTrackingRequest(int id, TrackingRequestDtoInput input)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            TrackingRequest request = await trackingEntities.TrackingRequest.FindAsync(id);
            if (request == null)
            {
                return NotFound();
            }
            if (request.DeviceInfo.UserId != UserId)
            {
                return Unauthorized();
            }

            DeviceInfo device = await trackingEntities.DeviceInfo.Where(d => d.Id == input.DeviceId).SingleOrDefaultAsync();
            if (device == null)
            {
                return this.UnprocessableEntity("No device with ID " + input.DeviceId + "was found");
            }
            if (device.UserId == UserId)
            {
                return BadRequest("You cannot track devices of your own user account");
            }

            TrackingRequest duplicate = await GetTrackingRequestByDeviceId(input.DeviceId);
            if (duplicate != null && duplicate.Id != request.Id)
            {
                return Conflict();
            }

            Mapper.Map(input, request);
            trackingEntities.Entry(request).State = EntityState.Modified;
            await trackingEntities.SaveChangesAsync();

            var output = Mapper.Map<TrackingRequestDtoOutput>(request);
            return Ok(output);
        }

        // POST: api/TrackingRequests
        /// <summary>
        /// Try to track another user by creating a tracking request.
        /// If the authenticated user has already created a tracking request for the specified device the existing tracking request will be edited instead.
        /// Returns 201 for creation and 200 for update.
        /// </summary>
        [ResponseType(typeof(TrackingRequestDtoOutput))]
        public async Task<IHttpActionResult> PostTrackingRequest(TrackingRequestDtoInput input)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            DeviceInfo device = await trackingEntities.DeviceInfo.Where(d => d.Id == input.DeviceId).SingleOrDefaultAsync();
            if (device == null)
            {
                return this.UnprocessableEntity("No device with ID " + input.DeviceId + "was found");
            }
            if (device.UserId == UserId)
            {
                return BadRequest("You cannot track devices of your own user account");
            }

            bool created = false;
            TrackingRequest request = await GetTrackingRequestByDeviceId(input.DeviceId);
            if (request == null)
            {
                created = true;
                request = Mapper.Map<TrackingRequest>(input);
                trackingEntities.TrackingRequest.Add(request);
            }
            else
            {
                Mapper.Map(input, request);
                trackingEntities.Entry(request).State = EntityState.Modified;
            }
            
            await trackingEntities.SaveChangesAsync();

            var output = Mapper.Map<TrackingRequestDtoOutput>(request);
            if (created)
            {
                return CreatedAtRoute("DefaultApi", new {id = request.Id}, output);
            }
            return Ok(output);
        }

        private Task<TrackingRequest> GetTrackingRequestByDeviceId(int deviceId)
        {
            return trackingEntities.TrackingRequest.SingleOrDefaultAsync(p => p.DeviceInfoId == deviceId && p.UserId == UserId);
        }

        // DELETE: api/TrackingRequests/5
        /// <summary>
        /// Delete the tracking request with the specified ID. 
        /// If the authenticated user did not create the the tracking request the request will fail.
        /// </summary>
        [ResponseType(typeof(TrackingRequestDtoOutput))]
        public async Task<IHttpActionResult> DeleteTrackingRequest(int id)
        {
            TrackingRequest request = await trackingEntities.TrackingRequest.FindAsync(id);
            if (request == null)
            {
                return NotFound();
            }
            if (request.DeviceInfo.UserId != UserId)
            {
                return Unauthorized();
            }

            trackingEntities.TrackingRequest.Remove(request);
            await trackingEntities.SaveChangesAsync();

            var output = Mapper.Map<TrackingRequestDtoOutput>(request);
            return Ok(output);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                trackingEntities.Dispose();
                userEntities.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}