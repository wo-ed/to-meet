﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using Microsoft.AspNet.Identity;
using ToMeet.Models;

namespace ToMeet.Controllers
{
    [Authorize]
    public class InvitationsController : ApiController
    {
        private ToMeetEntities trackingEntities = new ToMeetEntities();
        private ApplicationDbContext userEntities = new ApplicationDbContext();

        private string UserId => User.Identity.GetUserId();

        // GET: api/Invitations
        /// <summary>
        /// Get all invitations created by or given to the authenticated user.
        /// </summary>
        public async Task<IEnumerable<InvitationDtoOutput>> GetInvitation()
        {
            var createdInvitations = trackingEntities.Invitation.Where(i => i.Event.HostUserId == UserId);
            var grantedInvitations = trackingEntities.Invitation.Where(i => i.GuestUserId == UserId);
            var combinedInvitations = await createdInvitations.Union(grantedInvitations).ToListAsync();
            return combinedInvitations.Select(Mapper.Map<InvitationDtoOutput>);
        }

        // GET: api/Invitations/5
        /// <summary>
        /// Get the invitation with the specified ID. 
        /// If the authenticated user is neither the host nor the guest the request will fail.
        /// </summary>
        [ResponseType(typeof(InvitationDtoOutput))]
        public async Task<IHttpActionResult> GetInvitation(int id)
        {
            Invitation invitation = await trackingEntities.Invitation.FindAsync(id);
            if (invitation == null)
            {
                return NotFound();
            }
            if (invitation.Event.HostUserId != UserId && invitation.GuestUserId != UserId)
            {
                return Unauthorized();
            }

            var output = Mapper.Map<InvitationDtoOutput>(invitation);
            return Ok(output);
        }

        // POST: api/Invitations
        /// <summary>
        /// Invite another user to an event by creating an Invitation.
        /// If the authenticated user has already created an invitation for the specified event the existing invitation will be edited instead.
        /// Returns 201 for creation and 200 for update.
        /// </summary>
        [ResponseType(typeof(InvitationDtoOutput))]
        public async Task<IHttpActionResult> PostInvitation(InvitationDtoInput input)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var ev = await trackingEntities.Event.FindAsync(input.EventId);
            if (ev == null)
            {
                return this.UnprocessableEntity("No Event with ID " + input.EventId + " was found");
            }
            if (ev.HostUserId != UserId)
            {
                return Unauthorized();
            }
            if (input.GuestUserName.Trim() == User.Identity.Name)
            {
                return BadRequest("You cannot invite yourself");
            }

            ApplicationUser guestUser = await GetUserByName(input.GuestUserName);
            if (guestUser == null)
            {
                return this.UnprocessableEntity("No User with name " + input.GuestUserName + " was found");
            }

            bool created = false;
            var invitation = await GetInvitation(input.EventId, guestUser.Id);
            if (invitation == null)
            {
                created = true;
                invitation = Mapper.Map<Invitation>(input);
                trackingEntities.Invitation.Add(invitation);
            }
            else
            {
                Mapper.Map(input, invitation);
                trackingEntities.Entry(invitation).State = EntityState.Modified;
            }

            await trackingEntities.SaveChangesAsync();

            var output = Mapper.Map<InvitationDtoOutput>(invitation);
            if (created)
            {
                return CreatedAtRoute("DefaultApi", new { id = invitation.Id }, output);
            }
            return Ok(output);
        }

        private async Task<ApplicationUser> GetUserByName(string guestUserName)
        {
            return await userEntities.Users.SingleOrDefaultAsync(u => u.UserName == guestUserName);
        }

        private Task<Invitation> GetInvitation(int eventId, string guestUserId)
        {
            return trackingEntities.Invitation.SingleOrDefaultAsync(i => i.GuestUserId == guestUserId && i.EventId == eventId);
        }

        // DELETE: api/Invitations/5
        /// <summary>
        /// Delete the invitation with the specified ID. 
        /// If the authenticated user did not create the invitation and is not the guest the request will fail.
        /// </summary>
        [ResponseType(typeof(InvitationDtoOutput))]
        public async Task<IHttpActionResult> DeleteInvitation(int id)
        {
            Invitation invitation = await trackingEntities.Invitation.FindAsync(id);
            if (invitation == null)
            {
                return NotFound();
            }
            if (invitation.Event.HostUserId != UserId && invitation.GuestUserId != UserId)
            {
                return Unauthorized();
            }

            trackingEntities.Invitation.Remove(invitation);
            await trackingEntities.SaveChangesAsync();

            var output = Mapper.Map<InvitationDtoOutput>(invitation);
            return Ok(output);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                trackingEntities.Dispose();
                userEntities.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}