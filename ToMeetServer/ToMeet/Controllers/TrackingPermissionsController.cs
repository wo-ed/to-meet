﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Security;
using AutoMapper;
using Microsoft.AspNet.Identity;
using ToMeet.Models;

namespace ToMeet.Controllers
{
    [Authorize]
    public class TrackingPermissionsController : ApiController
    {
        private ToMeetEntities trackingEntities = new ToMeetEntities();
        private ApplicationDbContext userEntities = new ApplicationDbContext();

        private string UserId => User.Identity.GetUserId();


        // GET: api/TrackingPermissions
        /// <summary>
        /// Get all permissions created by or given to the authenticated user.
        /// </summary>
        public async Task<IEnumerable<TrackingPermissionDtoOutput>> GetTrackingPermission()
        {
            var createdPermissions = trackingEntities.TrackingPermission.Where(p => p.DeviceInfo.UserId == UserId);
            var grantedPermissions = trackingEntities.TrackingPermission.Where(p => p.UserId == UserId);
            var combinedPermissions = await createdPermissions.Union(grantedPermissions).ToListAsync();
            return combinedPermissions.Select(Mapper.Map<TrackingPermissionDtoOutput>);
        }

        // GET: api/TrackingPermissions/5
        /// <summary>
        /// Get the permission with the specified ID. 
        /// If the authenticated user did not create the permission and is not the subject of the permission the request will fail.
        /// </summary>
        [ResponseType(typeof(TrackingPermissionDtoOutput))]
        public async Task<IHttpActionResult> GetTrackingPermission(int id)
        {
            TrackingPermission trackingPermission = await trackingEntities.TrackingPermission.FindAsync(id);
            if (trackingPermission == null)
            {
                return NotFound();
            }
            if (trackingPermission.DeviceInfo.UserId != UserId && trackingPermission.UserId != UserId)
            {
                return Unauthorized();
            }

            return Ok(trackingPermission);
        }

        // POST: api/TrackingPermissions
        /// <summary>
        /// Enable another user to track your device by creating a permission.
        /// If the authenticated user has already created a permission for the specified device the existing permission will be edited instead.
        /// Returns 201 for creation and 200 for update.
        /// </summary>
        [ResponseType(typeof(TrackingPermissionDtoOutput))]
        public async Task<IHttpActionResult> PostTrackingPermission(TrackingPermissionDtoInput input)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (input.UserName == User.Identity.Name)
            {
                return BadRequest("You cannot create permissions for yourself");
            }
            ApplicationUser friend = await GetUserByName(input.UserName);
            if (friend == null)
            {
                return this.UnprocessableEntity("No user with name " + input.UserName + " was found");
            }

            DeviceInfo device = await trackingEntities.DeviceInfo.FirstOrDefaultAsync(
                d => d.MACBluetooth == input.MACBluetooth && d.UserId == UserId);
            if (device == null)
            {
                device = new DeviceInfo
                {
                    UserId = this.UserId,
                    LastLatitude = "",
                    LastLongitude = "",
                    LastDate = DateTime.Now,
                    MACBluetooth = input.MACBluetooth
                };
                trackingEntities.DeviceInfo.Add(device);
                try
                {
                    await trackingEntities.SaveChangesAsync();
                }
                catch
                {
                    return this.UnprocessableEntity("Failed to create device with MacBluetooth " + input.MACBluetooth);
                }
            }
            else if (device.UserId != UserId)
            {
                return Unauthorized();
            }

            bool created = false;
            TrackingPermission permission = await GetTrackingPermission(device.Id, friend.Id);
            if (permission == null)
            {
                created = true;
                permission = Mapper.Map<TrackingPermission>(input);
                trackingEntities.TrackingPermission.Add(permission);
            }
            else 
            {
                Mapper.Map(input, permission);
                trackingEntities.Entry(permission).State = EntityState.Modified;
            }
            permission.DeviceInfoId = device.Id;
            
            await trackingEntities.SaveChangesAsync();

            var output = Mapper.Map<TrackingPermissionDtoOutput>(permission);
            if (created)
            {
                return CreatedAtRoute("DefaultApi", new {id = permission.Id}, output);
            }
            return Ok(output);
        }

        private async Task<ApplicationUser> GetUserByName(string userName)
        {
            return await userEntities.Users.SingleOrDefaultAsync(u => u.UserName == userName);
        }

        private Task<TrackingPermission> GetTrackingPermission(int deviceId, string friendUserId)
        {
            return trackingEntities.TrackingPermission.SingleOrDefaultAsync(p => p.DeviceInfoId == deviceId && p.UserId == friendUserId);
        }

        // DELETE: api/TrackingPermissions/5
        /// <summary>
        /// Delete the permission with the specified ID. 
        /// If the authenticated user did not create the permission the request will fail.
        /// </summary>
        [ResponseType(typeof(TrackingPermissionDtoOutput))]
        public async Task<IHttpActionResult> DeleteTrackingPermission(int id)
        {
            TrackingPermission permission = await trackingEntities.TrackingPermission.FindAsync(id);
            if (permission == null)
            {
                return NotFound();
            }
            if (permission.DeviceInfo.UserId != UserId)
            {
                return Unauthorized();
            }

            trackingEntities.TrackingPermission.Remove(permission);
            await trackingEntities.SaveChangesAsync();

            var output = Mapper.Map<TrackingPermissionDtoOutput>(permission);
            return Ok(output);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                trackingEntities.Dispose();
                userEntities.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}