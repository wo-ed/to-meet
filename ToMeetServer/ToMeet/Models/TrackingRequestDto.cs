﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToMeet.Models
{
    public class TrackingRequestDtoInput
    {
        [Required]
        public int DeviceId { get; set; }
        [Required]
        public bool TrackGPS { get; set; }
        [Required]
        public bool TrackBluetooth { get; set; }
    }

    public class TrackingRequestDtoOutput
    {
        public int Id { get; set; }
        public int DeviceId { get; set; }
        public bool TrackGPS { get; set; }
        public bool TrackBluetooth { get; set; }
    }
}