﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToMeet.Models
{
    public class InvitationDtoInput
    {
        [Required]
        public int EventId { get; set; }
        [Required]
        public string GuestUserName { get; set; }
    }

    public class InvitationDtoOutput
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public EventDtoOutput Event { get; set; }
        public string GuestUserName { get; set; }
    }
}