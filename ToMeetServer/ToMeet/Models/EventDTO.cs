﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ToMeet.Models
{
    public class EventDtoInput
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public string Latitude { get; set; }
        [Required]
        public string Longitude { get; set; }
        [Required]
        public Nullable<System.DateTime> Date { get; set; }
        public string Info { get; set; }
        public ISet<string> InvitedUsers { get; set; }
    }

    public class EventDtoOutput
    {
        public int Id { get; set; }
        public string HostUserName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Info { get; set; }
        public IEnumerable<string> InvitedUsers { get; set; }
        public int InvitationId { get; set; } = -1;
    }
}