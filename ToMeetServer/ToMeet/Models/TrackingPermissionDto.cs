﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToMeet.Models
{
    public class TrackingPermissionDtoInput
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string MACBluetooth { get; set; }
        [Required]
        public bool GPSPermissionGranted { get; set; }
        [Required]
        public bool MACPermissionGranted { get; set; }
    }

    public class TrackingPermissionDtoOutput
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public bool GPSPermissionGranted { get; set; }
        public bool MACPermissionGranted { get; set; }
        public int DeviceId { get; set; }
        public DeviceDtoOutput Device { get; set; }
    }
}