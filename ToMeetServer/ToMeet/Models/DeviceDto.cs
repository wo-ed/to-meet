﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToMeet.Models
{
    public class DeviceDtoInput
    {
        [Required]
        public string MACBluetooth { get; set; }
        public string LastLatitude { get; set; }
        public string LastLongitude { get; set; }
        public Nullable<System.DateTime> LastDate { get; set; }
        public string FcmToken { get; set; }
    }

    public class DeviceDtoOutput
    {
        public int Id { get; set; }

        /// <summary>
        /// The device owner's user name
        /// </summary>
        public string UserName { get; set; }
        public string MACBluetooth { get; set; }
        public string LastLatitude { get; set; }
        public string LastLongitude { get; set; }
        public Nullable<System.DateTime> LastDate { get; set; }
    }
}